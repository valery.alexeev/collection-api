<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\LinkController;
use App\Http\Controllers\EmailController;
use App\Http\Controllers\DigestController;
use App\Http\Controllers\CategoryController;
use App\Http\Controllers\CollectionApiController;

$proxy_url    = getenv('PROXY_URL');
$proxy_schema = getenv('PROXY_SCHEMA');

if (!empty($proxy_url)) {
   URL::forceRootUrl($proxy_url);
}

if (!empty($proxy_schema)) {
   URL::forceScheme($proxy_schema);
}


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
   return view('welcome');
});

Route::get('/set/{set}', function (Request $request, \App\Models\Set $set) {
   return view('set.show', ['set' => $set]);
});

Route::get('/set/{set}/images', function (Request $request, \App\Models\Set $set) {
   return view('set.images', ['links' => $set->links]);
});

Route::get('/telegram-publish/{link}', [LinkController::class, 'telegram_publish'])->middleware('auth');





Route::group(['prefix' => 'collection-api', 'middleware' => ['respond.json']], function () {
   Route::get('navigation', [CollectionApiController::class, 'navigation']);
   Route::get('latest', [CollectionApiController::class, 'latest']);
   // Route::get('categories', [CollectionApiController::class, 'categories']);
   Route::get('category/{category}', [CollectionApiController::class, 'category']);
   Route::get('category/{category}/links', [CollectionApiController::class, 'links']);


   Route::get('favourites', [CollectionApiController::class, 'getAllFavourites']);
   Route::post('favourites/{link}/add', [CollectionApiController::class, 'addToFavourites']);
   Route::post('favourites/{link}/remove', [CollectionApiController::class, 'removeFromFavourites']);

   Route::get('set/{set}/links', [CollectionApiController::class, 'setLinks']);
});



Route::group(['prefix' => 'api', 'middleware' => ['respond.json']], function () {
   Route::resource('category', CategoryController::class)->only('index', 'show');
   Route::group(['prefix' => 'category/{category}'], function () {
      Route::resource('link', LinkController::class)->only('index', 'show', 'store', 'destroy');
   });


   Route::group(['prefix' => 'sh'], function () {
      Route::get('categories',      [CategoryController::class,   'sh_index']);
      Route::post('link',           [LinkController::class,       'sh_store']);
      Route::delete('link/{link}',  [LinkController::class,       'sh_destroy']);
      Route::get('links',           [LinkController::class,       'sh_index']);
   });
});

Route::resource('digest', DigestController::class);
Route::get('digest/{digest}/cover', [DigestController::class, 'cover']);
Route::get('digest/{digest}/stat', [DigestController::class, 'stat']);
// Route::get('digest/{digest}/stat', [DigestController::class, 'stat'])->middleware(['auth']);
Route::get('email/{email}', [EmailController::class, 'show']);

Route::get('link', [LinkController::class, 'index']);
Route::get('link/{link}/images', [LinkController::class, 'images']);
Route::get('link/{link}/go', [LinkController::class, 'go'])->name('link.go');

// Route::get('builder', [LinkController::class, 'builder']);