<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLinkSetPivotTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('link_set', function (Blueprint $table) {
            $table->unsignedBigInteger('link_id')->index();
            $table->foreign('link_id')->references('id')->on('links')->onDelete('cascade');
            $table->unsignedBigInteger('set_id')->index();
            $table->foreign('set_id')->references('id')->on('sets')->onDelete('cascade');
            $table->primary(['link_id', 'set_id']);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('link_set');
    }
}
