<?php
namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UsersTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        DB::table('users')->delete();
        
        DB::table('users')->insert(array (
            0 => 
            array (
                'created_at' => '2020-10-28 08:23:43',
                'email' => 'valery.alexeev@me.com',
                'email_verified_at' => NULL,
                'id' => 1,
                'name' => 'Valery Alexeev',
                'password' => '$2y$10$3MauUrvKAU2WvbXvnf5gr.lUjHRCawJ9xHVeQn9Nd3DE7aZC7Boty',
                'remember_token' => NULL,
                'updated_at' => '2020-10-28 08:23:43',
            ),
        ));
        
        
    }
}