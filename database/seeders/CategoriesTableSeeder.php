<?php

use Illuminate\Database\Seeder;

class CategoriesTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('categories')->delete();
        
        \DB::table('categories')->insert(array (
            0 => 
            array (
                'id' => 1,
                'title' => 'Полезные ресурсы',
                'image' => NULL,
                '_lft' => 1,
                '_rgt' => 8,
                'parent_id' => NULL,
                'created_at' => '2020-10-28 12:59:16',
                'updated_at' => '2020-10-28 12:59:16',
            ),
            1 => 
            array (
                'id' => 2,
                'title' => 'Иконки',
                'image' => NULL,
                '_lft' => 2,
                '_rgt' => 3,
                'parent_id' => 1,
                'created_at' => '2020-10-28 12:59:33',
                'updated_at' => '2020-10-28 13:04:50',
            ),
            2 => 
            array (
                'id' => 3,
                'title' => 'Книги',
                'image' => NULL,
                '_lft' => 9,
                '_rgt' => 10,
                'parent_id' => NULL,
                'created_at' => '2020-10-28 13:07:20',
                'updated_at' => '2020-10-28 13:07:20',
            ),
            3 => 
            array (
                'id' => 4,
                'title' => 'Курсы и лекции',
                'image' => NULL,
                '_lft' => 11,
                '_rgt' => 12,
                'parent_id' => NULL,
                'created_at' => '2020-10-28 13:07:29',
                'updated_at' => '2020-10-28 13:07:29',
            ),
            4 => 
            array (
                'id' => 5,
                'title' => 'Полезные статьи',
                'image' => NULL,
                '_lft' => 13,
                '_rgt' => 14,
                'parent_id' => NULL,
                'created_at' => '2020-10-28 13:07:39',
                'updated_at' => '2020-10-28 13:07:39',
            ),
            5 => 
            array (
                'id' => 6,
                'title' => 'Инструменты',
                'image' => NULL,
                '_lft' => 4,
                '_rgt' => 5,
                'parent_id' => 1,
                'created_at' => '2020-10-28 13:28:52',
                'updated_at' => '2020-10-28 13:28:52',
            ),
            6 => 
            array (
                'id' => 7,
                'title' => 'Плагины',
                'image' => NULL,
                '_lft' => 6,
                '_rgt' => 7,
                'parent_id' => 1,
                'created_at' => '2020-10-28 13:28:59',
                'updated_at' => '2020-10-28 13:29:31',
            ),
            7 => 
            array (
                'id' => 8,
                'title' => 'Интересные видео',
                'image' => NULL,
                '_lft' => 15,
                '_rgt' => 16,
                'parent_id' => NULL,
                'created_at' => '2020-10-28 17:51:05',
                'updated_at' => '2020-10-28 17:51:05',
            ),
            8 => 
            array (
                'id' => 9,
                'title' => 'Новости',
                'image' => NULL,
                '_lft' => 17,
                '_rgt' => 18,
                'parent_id' => NULL,
                'created_at' => '2020-10-28 17:51:14',
                'updated_at' => '2020-10-28 17:51:14',
            ),
            9 => 
            array (
                'id' => 10,
                'title' => 'Вдохновение',
                'image' => NULL,
                '_lft' => 19,
                '_rgt' => 20,
                'parent_id' => NULL,
                'created_at' => '2020-10-28 17:51:14',
                'updated_at' => '2020-10-28 17:51:14',
            ),
        ));
        
        
    }
}