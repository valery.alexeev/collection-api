<?php

use Illuminate\Database\Seeder;

class LinksTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('links')->delete();
        
        \DB::table('links')->insert(array (
            0 => 
            array (
                'id' => 1,
                'title' => 'Использование реальных данных в Figma',
                'description' => 'Полезная статья о том, как при помощи готовых плагинов и Google-таблиц навсегда забыть про глупый Loren Ipsum и использовать в своих проектах реальные данные. Отлично помогает проводить стресс-тесты и тестировать макеты на актуальном контенте.',
                'favicon' => NULL,
                'url' => 'https://ux.pub/ispolzovanie-realnyh-dannyh-v-figma/',
                'short_url' => NULL,
                'emoji' => NULL,
                'status' => 'draft',
                'published_at' => NULL,
                'category_id' => 5,
                'created_at' => '2020-10-28 18:15:40',
                'updated_at' => '2020-10-28 18:15:40',
            ),
            1 => 
            array (
                'id' => 2,
                'title' => 'Anima 4.0',
                'description' => 'Приложение для автоматической трансформации макетов в готовый код на базе React-компонентов получило очередное обновление. Обещают ещё более приятный код на выходе. Работает с Figma, Sketch и Adobe XD.',
                'favicon' => NULL,
                'url' => 'https://www.producthunt.com/posts/anima-4-0',
                'short_url' => NULL,
                'emoji' => NULL,
                'status' => 'draft',
                'published_at' => NULL,
                'category_id' => 6,
                'created_at' => '2020-10-28 18:20:03',
                'updated_at' => '2020-10-28 18:20:03',
            ),
            2 => 
            array (
                'id' => 3,
                'title' => 'Как разрабатывают накладные костюмы для актёров?',
                'description' => 'Занятное видео, рассказывающее о том, как устроен процесс создания накладных костюмов для кино-персонажей. Главная задача такого костюма — выглядеть на 100% натурально, как и его прототип.',
                'favicon' => NULL,
                'url' => 'https://www.facebook.com/watch/?v=375244210320461',
                'short_url' => NULL,
                'emoji' => NULL,
                'status' => 'draft',
                'published_at' => NULL,
                'category_id' => 8,
                'created_at' => '2020-10-28 18:31:41',
                'updated_at' => '2020-10-28 18:31:41',
            ),
            3 => 
            array (
                'id' => 4,
                'title' => 'MagicPattern',
                'description' => 'Генератор паттернов на чистом CSS. Имеет кучу ползунков в настройках и умеет изменять основной цвет графики.',
                'favicon' => NULL,
                'url' => 'https://www.magicpattern.design/tools/css-backgrounds?ref=webdesignernews.com',
                'short_url' => NULL,
                'emoji' => NULL,
                'status' => 'draft',
                'published_at' => NULL,
                'category_id' => 6,
                'created_at' => '2020-10-28 18:37:20',
                'updated_at' => '2020-10-28 18:37:20',
            ),
            4 => 
            array (
                'id' => 5,
                'title' => 'Archetype',
            'description' => 'Потрясный инструмент для построения типографической сетки с последующим экспортом CSS-кода. Умеет не просто задавать размеры и отступы для отдельных элементов, но также позволяет настраивать пары соседствующих элементов (например, параграф + H3). Однозначно, крутая штука!',
                'favicon' => NULL,
                'url' => 'https://archetypeapp.com/#',
                'short_url' => NULL,
                'emoji' => NULL,
                'status' => 'draft',
                'published_at' => NULL,
                'category_id' => 6,
                'created_at' => '2020-10-28 18:40:13',
                'updated_at' => '2020-10-28 18:40:13',
            ),
            5 => 
            array (
                'id' => 6,
                'title' => 'Интерактивные сайты',
                'description' => 'Мега-подборка лучших, по-настоящему «живых» и затягивающих сайтов. Залип на каждом из них по несколько минут: прямо какой-то детский восторг. Бесподобная графика, приятный sound-дизайн и самое главное — каждый проект сделан с душой.',
                'favicon' => NULL,
                'url' => 'https://qodeinteractive.com/magazine/playable-interactive-websites/?ref=webdesignernews.com#gucci-off-the-grid',
                'short_url' => NULL,
                'emoji' => NULL,
                'status' => 'draft',
                'published_at' => NULL,
                'category_id' => 10,
                'created_at' => '2020-10-28 18:49:47',
                'updated_at' => '2020-10-28 18:49:47',
            ),
            6 => 
            array (
                'id' => 7,
                'title' => 'Flag Stories',
            'description' => 'Сумасшедшая (по количеству проделанной работы) инфографика о флагах всех стран мира. Отличное пособие по визуализации данных, которые ранее до вас никто не визуализировал. Диаграмма с пропорциями используемых в флагах цветов покорила моё сердечко!',
                'favicon' => NULL,
                'url' => 'https://flagstories.co/',
                'short_url' => NULL,
                'emoji' => NULL,
                'status' => 'draft',
                'published_at' => NULL,
                'category_id' => 10,
                'created_at' => '2020-10-28 18:57:14',
                'updated_at' => '2020-10-28 18:57:14',
            ),
            7 => 
            array (
                'id' => 8,
                'title' => 'Radix Icons',
                'description' => 'Небольшая, но до боли приятная коллекция интерфейсных иконок. Забирать можно в любом удобном формате: копировать SVG в буфер обмена, импортировать в Figma или даже использовать в виде React-компонента.',
                'favicon' => NULL,
                'url' => 'https://icons.modulz.app/?ref=webdesignernews.com',
                'short_url' => NULL,
                'emoji' => NULL,
                'status' => 'draft',
                'published_at' => NULL,
                'category_id' => 2,
                'created_at' => '2020-10-28 19:06:35',
                'updated_at' => '2020-10-28 19:06:35',
            ),
            8 => 
            array (
                'id' => 9,
                'title' => 'uCalc',
                'description' => 'Удобный и приятный на вид конструктор калькуляторов для сайтов. Крайне популярный запрос у заказчиков: «хотим сделать калькулятор для расчёта стоимости чего угодно». А тут вы взяли и собрали без лишних вопросов.',
                'favicon' => NULL,
                'url' => 'https://ucalc.pro/',
                'short_url' => NULL,
                'emoji' => NULL,
                'status' => 'draft',
                'published_at' => NULL,
                'category_id' => 6,
                'created_at' => '2020-10-28 19:11:36',
                'updated_at' => '2020-10-28 19:11:36',
            ),
            9 => 
            array (
                'id' => 10,
                'title' => '3D Lettering Pack',
                'description' => 'Красивущий набор графики для оформления текста няшном 3D. Чем-то напоминает длинные надувные шарики, из которых скрутили все буквы англоязычного алфавита. Можно неплохо использовать в оформлении лендингов.',
                'favicon' => NULL,
                'url' => 'https://www.artify.co/3d-lettering?ref=producthunt',
                'short_url' => NULL,
                'emoji' => NULL,
                'status' => 'draft',
                'published_at' => NULL,
                'category_id' => 1,
                'created_at' => '2020-10-28 19:19:27',
                'updated_at' => '2020-10-28 19:19:27',
            ),
        ));
        
        
    }
}