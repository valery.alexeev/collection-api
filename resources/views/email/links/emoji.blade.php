<tr>
    <td style="padding-top: 35px;">
        <table border="0" cellspacing="0" cellpadding="0" >
            @if (!empty($item['image']))
            <tr>
                <td colspan="2" style="padding-top: 15px; padding-bottom: 20px;">
                    <a href="{{ $item['url'] }}" target="_blank"><img src="{{ env('APP_URL') }}{{ Storage::url($item['image']) }}" style="border-radius: 8px; width: 100%; height: auto;"></a>
                </td>
            </tr>
            @endif
            <tr>
                <td class="item__emoji" width="45" style="font-size: 36px; padding-top: 5px;" valign="top">
                    {!! emoji(!empty($item['emoji']) ? $item['emoji'] : '📎') !!}
                </td>
                <td style="line-height: 26px; font-size: 16px; padding-left: 15px;">
                    <p style="margin-bottom: 5px; margin-top: 0px; line-height: 26px; font-size: 16px;">
                        @if (!empty($item['url']))
                        <a target="_blank" href="{{ $item['url'] }}" style="color: #689317; font-weight: 700;" >{{ \App\Models\Link::generateDotTitle($item['title']) }}</a> 
                        @else
                        <span style="color: black; font-weight: 700;" >
                            {{ \App\Models\Link::generateDotTitle($item['title']) }}
                        </span>
                        @endif
                        {!! typograf($item['description']) !!}
                    </p>
                    @if (!empty($item['url']))
                    <a href="{{ $item['url'] }}" style="opacity: 0.5; color: black; font-size: 12px;" target="_blank">{{ \App\Models\Link::extractDomain($item['url']) }}</a>
                    @endif
                </td>
            </tr>
        </table>
    </td>
</tr>