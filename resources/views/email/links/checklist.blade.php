<tr>
    <td style="padding-top: 15px;">
        <table border="0" cellspacing="0" cellpadding="0" >
            <tr>
                <td class="item__index" width="36" style="line-height: 26px; width: 36px; font-size: 14px; opacity: 0.5; padding-top: 4px;" valign="top">
                    <div style="width: 20px; height: 20px; background-color: white; border-radius: 2px; border: 2px solid #689317;"></div>
                </td>
                <td style="line-height: 26px; font-size: 14px;" valign="top">
                    <p style="margin-bottom: 0px; margin-top: 0px; line-height: 26px; font-size: 14px;">{{ $item['title'] }} <span style="font-style: italic; opacity: .6;">{{ $item['description'] }}</span> @if(!empty($item['url']))<a style="color: #689317;" target="_blank" href="{{ $item['url'] }}">Подробнее →</a>@endif</p>
                </td>
            </tr>
        </table>
    </td>
</tr>