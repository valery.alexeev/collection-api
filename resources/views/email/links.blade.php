<tr class="section section--{{ $block['color'] == '#F9FAF7' ? 'light' : 'white' }}">
    <td style="background-color: {{ $block['color'] ?? 'white' }}; padding-top: 80px; padding-bottom: 80px;">
        <table align="center" border="0" cellspacing="0" cellpadding="0" style="width: 90%; max-width: 660px">

            @include('email.heading', ['title' => $block['title'], 'subtitle' => $block['description']])

            @foreach ($block['links'] as $link)
                @include('email.links.' . $block['layout'], ['item' => $link['attributes'], 'index' => $loop->iteration])
            @endforeach

        </table>
    </td>
</tr>

