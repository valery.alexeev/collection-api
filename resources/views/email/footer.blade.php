<tr class="footer">
    <td style="background-color: #333; font-size: 14px; padding-top: 35px; padding-bottom: 40px;">
        <table align="center" border="0" cellspacing="0" cellpadding="0" style="width: 90%; max-width: 660px">
            <tr style="text-align: center;">
                <td style="color: #ccc; line-height: 1.5">
                    <p style="font-size: 42px; margin-top: 0px; margin-bottom: 8px; line-height: 42px;">{!! emoji('🤔', 42) !!}</p>
                    <p style="font-weight: 700; margin-top: 0px; margin-bottom: 8px; color: white;">Уже уходите..?</p>
                    <p style=" margin-top: 0px; margin-bottom: 16px;">Вы получили это письмо, так как оставили свою почту и согласие на рассылку от Школы #VA</p>
                <p style=" margin-top: 0px; margin-bottom: 0px;"><strong>Хотите получать меньше писем от нас?</strong><br> Вы можете <a style="color: white;" href='@{{unsubscribe_url}}'>отписаться</a> в один клик.</p>
                </td>
            </tr>
        </table>
    </td>
</tr>