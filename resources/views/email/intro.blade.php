<tr class="section section--{{ $block['color'] == '#F9FAF7' ? 'light' : 'white' }}">
    <td style="background-color: {{ $block['color'] }}; padding-top: 60px; padding-bottom: 80px;">
        <table align="center" border="0" cellspacing="0" cellpadding="0" style="width: 90%; max-width: 660px">
            <tr>
                <td colspan="2" style="padding-bottom: 10px;">
                    <div style="font-size: 64px; line-height: 64px; padding-bottom: 10px;">{!! emoji($block['emoji'], 72) !!}</div>
                    <h1 style="margin-bottom: 0px; margin-top:10px; font-size: 32px;">{{ $block['title'] }}</h1>
                    <p style="font-size: 16px; line-height: 1.5; font-family: Inter, Arial, sans-serif; margin-bottom: 20px;">{{ $block['description'] }}</p>
                </td>
            </tr>

            @if ($block['show_author'])
            <tr>
                <td style="width: 72px;"  valign="middle">
                    <img src="{{ env('APP_URL') }}/author.png" style="width: 60px; height: auto">
                </td>
                <td valign="middle">
                    <p style="margin-top: 0px; margin-bottom: 0px;"><strong>Валерий Алексеев</strong></p>
                    <p style="margin-top: 5px; font-size: 12px; margin-bottom: 0px; opacity: 0.5">UX/UI дизайнер, руководитель Школы #VA</p>
                </td>
            </tr>
            @endif
            
            @if (!empty($block['intro_text']))
            <tr>
                <td colspan="2" style="padding-top: 25px;">
                    <p style="background-color: #F9FAF7; border: 1px solid #689317; padding: 28px 35px; border-radius: 8px; margin-top: 0px; margin-bottom: 0px; font-size: 15px; line-height: 1.6;">{{ $block['intro_text'] }}</p>
                </td>
            </tr>
            @endif
        </table>
    </td>
</tr>