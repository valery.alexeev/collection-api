<style>
li:empty {
    display: none;
}

body {
    background-color: #f6f6f6;
}

#content {
    width: 800px;
    padding: 50px 70px 70px;
    margin: 100px auto;
    background-color: white;
}

.copy {
    position: absolute;
    top: 10px;
    left: 50%;
    width: 150px;
    margin-left: -75px;
}
</style>

<script>function CopyToClipboard(e){if(document.selection)(t=document.body.createTextRange()).moveToElementText(document.getElementById(e)),t.select().createTextRange(),document.execCommand("copy");else if(window.getSelection){var t;(t=document.createRange()).selectNode(document.getElementById(e)),window.getSelection().addRange(t),document.execCommand("copy"),alert("Text has been copied, now paste in the text-area")}}</script>
<button class="copy" onclick="CopyToClipboard('content')">Скопировать</button>

<div id="content">
<h1>Дизайнерский дайджест за неделю: статьи, кейсы и полезные инструменты</h1>

<p>Ещё один дизайнерский дайджест с полезными ссылками, инструментами, кейсами и статьями. Располагайтесь поудобнее, будет интересно!</p>

@if (!empty($digest->content['intro']))
<p>{{ $digest->content['intro'] }}</p>
@endif

<p>Обязательно делитесь в комментариях своими мыслями по поводу новостей и материалов за прошедшую неделю. Мне будет полезно узнать ваше мнение. Вы также можете <a href="https://vc.ru/u/15534-valeriy-alekseev">подписаться на меня</a> здесь на VC, чтобы не пропустить новый дайджест в следующий раз.</p>




@if (!empty($digest->data['tools']))
<h2>Полезные инструменты</h2>
<p>Подборка приложений и сервисов, которые упростят твою жизнь</p>
    @foreach ($digest->data['tools'] as $item)
        @if (!empty($item->image))
            https://app.5steps.design{{ Storage::url($item->image) }}
        @endif
        <p>{{ $item->emoji ?? '⚙️' }} <strong><a href="{{ $item->url }}">{{ $item->title_dot }}</a></strong> {{ $item->description }}<p>
    @endforeach
@endif




@if (!empty($digest->data['articles']))
<hr>
<h2>Статьи и публикации</h2>
<p>Полезные материалы для чтения на русском и английском языке</p>
@foreach ($digest->data['articles'] as $item)
    @if (!empty($item->image))
        https://app.5steps.design{{ Storage::url($item->image) }}
    @endif
    <p>{{ $item->emoji ?? '📄' }} <strong>{{ $item->title_dot }}</strong> {{ $item->description }} <a href="{{ $item->url }}">Читать статью →</a></p>
@endforeach
@endif




@if (!empty($digest->data['news']))
<hr>
<h2>Новости одной строкой</h2>
<p>Важные новости из мира дизайна, маркетинга и бизнеса</p>
@foreach ($digest->data['news'] as $item)
<p><a href="{{ $item->url }}"><b>{{ $item->title_dot }}</b></a> {{ $item->description }}</p>
@endforeach
@endif


@if (!empty($digest->data['video']))
<hr>
<h2>Видео и лекции</h2>
<p>Познавательные материалы для изучения в свободное время</p>

@foreach ($digest->data['video'] as $item)
    <h3>{{ $item->title }}</h3> 
    <p>{{ $item->description }}</p>
    {{ $item->url }}
@endforeach

@endif




@if (!empty($digest->data['inspiration']))
<hr>
<h2>Вдохновение и кейсы</h2>
<p>Подборка крутых проектов для поиска новых идей и вдохновения</p>
    @foreach ($digest->data['inspiration'] as $item)
            @if (!empty($item->image))
                https://app.5steps.design{{ Storage::url($item->image) }}
            @endif
            <p><a href="{{ $item->url }}">{{ $item->emoji ?? '🎨' }} <strong>{{ $item->title_dot }}</strong></a> {{ $item->description }}</p>
    @endforeach
@endif



@if (!empty($digest->data['other']))
<hr>
<h2>Прочие ништяки и интересности</h2>
<p>Всё полезное и интересное, что не удалось разметить в других категориях</p>
<ol>
    @foreach ($digest->data['other'] as $item)
        <li>
            <a href="{{ $item->url }}"><b>{{ $item->title_dot }}</b></a> {{ $item->description }}
        </li>
    @endforeach
</ol>
@endif



<hr>

<h2>Заключение</h2>
<p>Обязательно делитесь в комментариях своими мыслями по поводу новостей и материалов за прошедшую неделю. Мне будет полезно узнать ваше мнение.</p>
<p>Не забывайте подписываться на <a href="https://s.va-promotion.ru/digest">Email-версию дайджеста</a> (она выходит немного раньше). Я также буду рад видеть вас в своём <a href="https://instagram.com/valery_alexeev">Instagram</a> и на <a href="https://youtube.com/va_promotion">YouTube</a>. Хороших выходных!</p>
</div>