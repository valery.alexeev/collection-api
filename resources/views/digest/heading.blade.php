<tr class="row--section-heading">
    <td style="padding-bottom: 10px;">
        <h2 style="font-size: 26px; margin-top: 0px; margin-bottom: 10px;">{{ $title }}</h2>
        <p style="font-size: 16px; opacity: 0.6; margin-top: 0px; margin-bottom: 0px; line-height: 1.5;">{{ $subtitle }}</p>
    </td>
</tr>