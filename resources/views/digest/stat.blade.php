<style>
.stat {
    border-collapse: collapse;
    text-align: left;
    width: 800px;
    max-width: 95%;
    margin: 50px auto;
    font-family: Arial;
    font-size: 16px;
}
.stat td, .stat th {
    border: 1px solid #ddd;
    padding: 10px 20px;
    font-size: 14px;
}
.stat .sub td, .stat .sub th {
    font-size: 0.8em;
    color: #666;
}
.stat .sub th {
    padding-left: 40px;
}
</style>

<table class="stat">
    @foreach ($stat as $link)
        <tr>
            <th>{{ $link->model->title }}</th>
            <th>{{ $link->clicks_count }}</th>
        </tr>
        @foreach ($link->refs as $ref => $count)
        <tr class="sub">
            <th>{{ $ref }}</th>
            <td>{{ $count }}</td>
        </tr>
        @endforeach
    @endforeach
</table>