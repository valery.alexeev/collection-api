<tr>
    <td style="padding-top: 35px;">
        <table border="0" cellspacing="0" cellpadding="0" >
            @if (!empty($item->image))
            <tr>
                <td colspan="2" style="padding-top: 15px; padding-bottom: 20px;">
                    <a href="{{ $item->go_link('email') }}" target="_blank"><img src="{{ env('APP_URL') }}{{ Storage::url($item->image) }}" style="border-radius: 8px; width: 100%; height: auto;"></a>
                </td>
            </tr>
            @endif
            <tr>
                <td class="item__emoji" width="50" style="font-size: 36px; padding-top: 5px;" valign="top">
                    {!! emoji($item->emoji ?? ($emoji ?? '📎'), 40) !!}
                </td>
                <td style="line-height: 26px; font-size: 16px; padding-left: 10px;">
                <p style="margin-bottom: 5px; margin-top: 0px; line-height: 26px; font-size: 16px;"><a style="color: #689317; font-weight: 700;" target="_blank" href="{{ $item->go_link('email') }}">{{ $item->title_dot }}</a> {!! typograf($item->description) !!}</p>
                    <a href="{{ $item->go_link('email') }}" style="opacity: 0.5; color: black; font-size: 12px;" target="_blank">{{ $item->domain }}</a>
                </td>
            </tr>
        </table>
    </td>
</tr>