<tr class="row-book">
    <td style="padding-right: 25px;" class="cell-mobile-full">
        <p style="margin-top: 0px; margin-bottom: 0px; text-transform: uppercase; letter-spacing: 0.1em; font-size: 12px; opacity: 0.5;">Полезная книга // {{ $book->extras->author ?? '' }}</p>
        <h2 style="max-width: 360px; font-weight: 700; font-size: 18px; line-height: 24px; margin-top: 15px; margin-bottom: 15px">{{ $book->title }}</h2>
        
        <div style="font-size: 14px; line-height: 22px; opacity: 0.6; margin-bottom: 25px;">
            {{ Illuminate\Mail\Markdown::parse($book->description) }}
        </div>

        <a href="{{ $book->go_link('email') }}" target="_blank" style="color: #689317;">Подробнее о книге →</a>
    </td>
    @if (!empty($book->image))
    <td style="width: 145px" valign="top" class="book-cell">
        <a href="{{ $book->go_link('email') }}" target="_blank">
            <img src="{{ env('APP_URL') }}{{ Storage::url($book->image) }}" style="width: 145px; height: auto;">
        </a>
    </td>
    @endif
</tr>