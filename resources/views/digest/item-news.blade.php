<tr>
    <td style="padding-top: 15px;">
        <table border="0" cellspacing="0" cellpadding="0" >
            <tr>
                <td class="item__index" width="25" style="line-height: 26px; font-size: 14px; opacity: 0.5;" valign="top">
                    {{ $index }}.
                </td>
                <td style="line-height: 26px; font-size: 14px;" valign="top">
                    <p style="margin-bottom: 5px; margin-top: 0px; line-height: 26px; font-size: 14px;">{{ $item->title_dot }} <span style="font-style: italic; opacity: .6;">{{ $item->description }}</span> <a style="color: #689317;" target="_blank" href="{{ $item->go_link('email') }}">Подробнее →</a></p>
                </td>
            </tr>
        </table>
    </td>
</tr>