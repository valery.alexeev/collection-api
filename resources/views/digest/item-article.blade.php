<tr>
    <td style="padding-top: 25px;">
        <table border="0" cellspacing="0" cellpadding="0" >
            @if (!empty($item->image))
            <tr>
                <td colspan="2" style="padding-top: 15px; padding-bottom: 20px;">
                    <a href="{{ $item->go_link('email') }}" target="_blank"><img src="{{ env('APP_URL') }}{{ Storage::url($item->image) }}" style="border-radius: 8px; width: 100%; height: auto;"></a>
                </td>
            </tr>
            @endif
            <tr>
                <td class="item__emoji" width="40" style="font-size: 24px; line-height: 24px; padding-top: 4px;" valign="top">
                    {!! emoji($item->emoji ?? ($emoji ?? '📎'), 24) !!}
                </td>
                <td style="line-height: 22px; font-size: 14px;">
                    <p style="margin-bottom: 5px; margin-top: 0px; line-height: 22px; font-size: 14px;"><a class="link-hover" style="color: black; text-decoration: none; font-weight: 700;" target="_blank" href="{{ $item->go_link('email') }}">{{ $item->title_dot }}</a> {!! typograf($item->description) !!} <a href="{{ $item->go_link('email') }}" style="color: #689317; text-decoration: none;" target="_blank">Читать -></a></p>
                </td>
            </tr>
        </table>
    </td>
</tr>