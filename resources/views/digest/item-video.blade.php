<tr>
    <td style="padding-top: 35px;">
        <table border="0" cellspacing="0" cellpadding="0" >
            @if (!empty($item->image))
            <tr>
                <td colspan="2" style="padding-bottom: 20px;">
                    <a href="{{ $item->go_link('email') }}" target="_blank"><img src="{{ env('APP_URL') }}{{ Storage::url($item->image) }}" style="border-radius: 6px; width: 240px; max-width: 60%; height: auto;"></a>
                </td>
            </tr>
            @endif
            <tr>
                <td style="line-height: 22px; font-size: 14px;">
                    <h3 style="margin-top: 0px; margin-bottom: 10px"><a class="link-hover" style="color: black; text-decoration: none; font-weight: 700;" target="_blank" href="{{ $item->go_link('email') }}">{{ $item->title }}</a></h3>
                    <p style="margin-bottom: 0px; margin-top: 0px; line-height: 22px; font-size: 14px;">{!! typograf($item->description) !!}</p>
                </td>
            </tr>
        </table>
    </td>
</tr>