<style>
.cover {
    background-color: #feebef;
    width: 1400px;
    height: 700px;
    overflow: hidden;
    position: relative;
}
.cover__image {
    width: 420px;
    height: 270px;
    object-fit: cover;
    object-position: top left;
    border-radius: 8px;
    box-shadow: 0px 10px 40px rgba(0,0,0,.08);
    transform: rotate(15deg);
    position: absolute;
    top: -130px;
    left: 750px;
    border: 8px solid white;
}

.cover__image:nth-of-type(1) {

}
.cover__image:nth-of-type(2) {
    top: 180px;
}
.cover__image:nth-of-type(3) {
    top: 500px;
}
.cover__image:nth-of-type(4) {
    left: 1190px;
    top: -15px;
}
.cover__image:nth-of-type(5) {
    left: 1190px;
    top: 300px;
}
.cover__image:nth-of-type(6) {
    left: 1190px;
    top: 620px;
}

h1 {
    font-family: "Roboto", sans-serif;
    font-size: 52px;
    font-weight: 700;
    width: 580px;
    margin: 90px;
}

ul {
    font-family: "Roboto", sans-serif;
    padding: 0px;
    font-size: 32px;
    list-style: none;
    position: absolute;
    bottom: 85px;
    left: 90px;
    line-height: 1.9;
    margin: 0px;
}
</style>

<div class="cover">
    <h1 contenteditable="true">Дизайнерский дайджест за неделю</h1>
    <ul>
        <li>Новости</li>
        <li>Статьи и видео</li>
        <li>Инструменты</li>
        <li>Вдохновение</li>
    </ul>
    @foreach ($images as $image)
    <img class="cover__image" src="https://app.5steps.design{{ Storage::url($image) }}">
    @endforeach
</div>