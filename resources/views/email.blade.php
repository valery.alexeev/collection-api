<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "https://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="https://www.w3.org/1999/xhtml">
    <head>
        <title>{{ $email->title }}</title>
        <meta http–equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta http–equiv="X-UA-Compatible" content="IE=edge" />
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <style type="text/css">
            .link-hover:hover {
				color: #689317 !important;
			}
            .video-thumb {
                position: relative;
                display: block;
            }
			@media screen and (max-width: 600px) {
				.row-book {
					display: flex;
					flex-direction: column;
				}
				.row-book > td {
					display:block; 
					box-sizing:border-box; 
					clear:both; 
					order: 2;
				}
				.row-book > td.book-cell {
					order: 1;
					padding-bottom: 30px;
				}
            }
            
            .text-block {
                line-height: 1.55;
            }

            .text-block blockquote p {
                margin: 0px !important;
                font-size: inherit !important;
            }
            .text-block blockquote p + p {
                margin-top: 15px !important;
            }
            .section--white + .section--white > td,
            .section--light + .section--light > td {
                padding-top: 0px !important;
            }
        </style>
</head>
<body style="margin:0px; padding:0px;" bgcolor="white">

	{{-- <p style="font-size: 0px; opacity: 0;">{{ $digest->content['intro'] ?? 'Ещё один дизайнерский дайджест с полезными ссылками, инструментами, кейсами и статьями. Располагайтесь поудобнее, будет интересно!' }}</p> --}}
	<table align="center" width="100%" border="0" cellspacing="0" cellpadding="0" class="main_table" style="font-family: Inter, Arial, sans-serif; width:100%;">

        @foreach ($blocks as $block)
            @include('email.' . $block['layout'], ['block' => $block['attributes']])
        @endforeach        

        @include('email.footer')
	</table>

	<link href="https://fonts.googleapis.com/css2?family=Inter:wght@400;700&display=swap" rel="stylesheet">
</body>
</html>