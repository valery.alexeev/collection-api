<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>{{ $set->title }}</title>

    {{-- <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/typebase.css/0.5.0/typebase.min.css"> --}}
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Inter&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="/fonts/stylesheet.css?v=12">

</head>
<body>
    
    <style>
        html {
            font-size: 16px;
        }
        @media screen and (max-width: 768px) {
            html {
                font-size: 14px;
            }
        }
        * {
            position: relative;
            padding: 0px;
            margin: 0px;
            box-sizing: border-box;
            -webkit-font-smoothing: antialiased;
        }

        body {
            background-color: white;
            font-family: Inter, sans-serif;
        }

        h1, h2, h3, h4, h5, h6 {
            font-family: "PP Pangram Sans", Inter, sans-serif;
        }

        h1 {
            font-size: 3.8rem;
            margin-top: 2rem;
        }

        h2 {
            font-size: 2.4rem;
        }

        p {
            font-size: 1.1rem;
            line-height: 165%;
            margin-top: 0.8rem;
        }

        @media screen and (max-width: 500px) {
            html {
                font-size: 12px;
            }
            p {
                font-size: 1.3rem;
            }
        }

        .set-content {
            width: 88%;
            max-width: 768px;
            margin: 0 auto;
            padding: 50px 0px 100px;
        }

        hr {
            border: none;
            background-color: #ddd;
            height: 1px;
            margin: 60px 0px;
        }

        .set-item img {
            width: 100%;
            height: auto;
        }

        .set-item h2 {
            margin-top: 2rem;
        }

        .set-item p {
            margin-top: 0.8rem;
        }

        .set-title {

        }

        .set-title h1 {

        }

        .set-title p.lead {
            font-size: 1.2rem;
            line-height: 165%;
        }

    </style>

<div class="set-content">
    <div class="set-title">
        <p><a href="https://instagram.com/valery_alexeev" target="_blank">@valery_alexeev</a></p>
        <h1>{!! typograf($set->title) !!}</h1>
        <p class="lead">{!! typograf($set->extras->short ?? '') !!}</p>
    </div>
    @foreach ($set->links as $link)
    <hr>
    <div class="set-item">
        <img src="https://app.5steps.design{{ Storage::url($link->image) }}">
        <h2>{!! typograf($link->title) !!}</h2>
        <p>{!! typograf($link->description) !!}</p>
        <p><a href="{{ $link->url }}" target="_blank">Перейти на сайт →</a></p>
    </div>
@endforeach
</div>

</body>
</html>