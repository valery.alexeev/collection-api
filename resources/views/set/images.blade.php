<link rel="preconnect" href="https://fonts.googleapis.com">
<link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
<link href="https://fonts.googleapis.com/css2?family=Inter&display=swap" rel="stylesheet">
<link rel="stylesheet" href="/fonts/stylesheet.css?v=12">

<style>
* {
    position: relative;
    padding: 0px;
    margin: 0px;
    box-sizing: border-box;
    -webkit-font-smoothing: antialiased;
}

body {
    background-color: #eee;
    font-family: Inter, sans-serif;
}

.gallery-image {
    width: 1200px;
    height: 1500px;
    padding: 100px 80px;
    display: flex;
    overflow: hidden;
    margin-bottom: 50px;
    flex-direction: column;
    justify-content: flex-start;
    align-items: flex-start;
}


.gallery-image__header {
    display: flex;
    justify-content: space-between;
    align-items: center;
    font-size: 30px;
    width: 100%;
    margin-bottom: 120px;
    opacity: .6;
}

.gallery-image__title {
    font-family: "PP Pangram Sans";
    font-size: 56px;
    margin: 0px;
}
.gallery-image__text {
    font-size: 32px;
    line-height: 1.55;
    margin-top: 16px;
}

.gallery-image__content {
    display: flex;
    justify-content: flex-start;
    align-items: flex-start;
    flex-direction: row;
    flex-wrap: nowrap;
}

.emoji__wrap {
    margin-left: 30px;
}
.emoji__wrap .emoji {
    max-width: 100px !important;
    font-size: 100px !important;
}

.gallery-image__cover__wrap {
    /* border-left: 0px; */
    width: 100%;
    flex-grow: 1;
    margin: 0px 0px 80px 0px;
}

.gallery-image__cover {
    border-radius: 30px;
    background-size: cover;
    background-position: center;
    width: 100%;
    height: 100%;
}


.gallery-image__domain {
    z-index: 100;
    display: inline-block;
    margin-top: 40px;
    font-weight: 600;
    font-size: 30px;
}

.gallery-image__domain svg {
    display: inline-block;
    vertical-align: top;
    width: 38px;
    height: 38px;
}

.result {
    width: 400px;
    height: auto;
    display: inline-block;
    margin: 0px 30px;
}

.image__wrap {
    width: 1200px;
}

.gallery-image__footer {
    display: flex;
    margin-top: 120px;
    justify-content: space-between;
    flex-direction: row;
    align-items: center;
    width: 100%;
}

.gallery-image__footer svg {
    height: 34px;
    width: auto;
    opacity: .5;
}




/* Чёрные слайды */
.image__wrap--1 .gallery-image {
    background-color: #181717;
    color: white;
}

.image__wrap--1 .gallery-image__footer svg:not(.fill) {
    stroke: white;
}

.image__wrap--1 .gallery-image__footer svg.fill {
    fill: white;
}

.image__wrap--1 .gallery-image__domain {
    color: #84FD31;
}

.image__wrap--1 .gallery-image__domain svg {
    fill: #84FD31;
}




/* Светлые слайды */
.image__wrap--2 .gallery-image {
    background-color: #F6FEF0;
    color: #110D02;
}

.image__wrap--2 .gallery-image__footer svg:not(.fill) {
    stroke: #110D02;
}

.image__wrap--2 .gallery-image__footer svg.fill {
    fill: #110D02;
}

.image__wrap--2 .gallery-image__domain {
    color: #53C32B;
}

.image__wrap--2 .gallery-image__domain svg {
    fill: #53C32B;
}



/* Зелёные слайды */
.image__wrap--3 .gallery-image {
    background-color: #C7FF81;
    color: #000000;
}
.image__wrap--3 .gallery-image__footer svg:not(.fill) {
    stroke: #000000;
}

.image__wrap--3 .gallery-image__footer svg.fill {
    fill: #000000;
}

.image__wrap--3 .gallery-image__domain {
    color: #4945F1;
}

.image__wrap--3 .gallery-image__domain svg {
    fill: #4945F1;
}
</style>


<div id="resutls"></div>

@php
    $rand = rand(0,2);
@endphp

@foreach ($links as $link)
<div class="image__wrap image__wrap--{{ ($loop->iteration + $rand) % 3 + 1 }}">
    <div class="gallery-image">
        <div class="gallery-image__header">
            <div class="gallery-image__header__left">@valery_alexeev</div>
            <div class="gallery-image__header__right">{{$loop->iteration + 1}}/{{ request()->total ?? 10 }}</div>
        </div>

        @if (!empty($link->image) && !request('short'))
        <div class="gallery-image__cover__wrap">
            <div class="gallery-image__cover" style="background-image: url(https://app.5steps.design{{ Storage::url($link->image) }})"></div>
        </div>
        @endif

        <div class="gallery-image__content">
            <div class="gallery-image__content--text">
                <div class="gallery-image__title">{!! typograf($link->title) !!}</div>
                <div class="gallery-image__text">{!! typograf($link->description) !!}</div>

                @if (!request('url') && !request('short'))
                <div class="gallery-image__domain">
                    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" width="24" height="24"><path fill="none" d="M0 0h24v24H0z"/><path d="M17.657 14.828l-1.414-1.414L17.657 12A4 4 0 1 0 12 6.343l-1.414 1.414-1.414-1.414 1.414-1.414a6 6 0 0 1 8.485 8.485l-1.414 1.414zm-2.829 2.829l-1.414 1.414a6 6 0 1 1-8.485-8.485l1.414-1.414 1.414 1.414L6.343 12A4 4 0 1 0 12 17.657l1.414-1.414 1.414 1.414zm0-9.9l1.415 1.415-7.071 7.07-1.415-1.414 7.071-7.07z"/></svg>
                    {{ getDomainFromURL($link->url) }}
                </div>
                @endif
            </div>
            {{-- @if (!empty($link->emoji))
            <div class="emoji__wrap">
                {!! emoji($link->emoji ?? ($emoji ?? '📎'), 50) !!}
            </div>
            @endif --}}
        </div>
        

        @if (!request('short'))
        <div class="gallery-image__footer">
            <div>

                @switch($loop->index % 4)
                    @case(0)
                        <svg width="35" height="30" viewBox="0 0 35 30" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <path d="M3.9932 28L17.4598 4.07717L30.9263 28H3.9932Z" stroke-width="4"/>
                        </svg>
                        @break

                    @case(1)
                        <svg width="31" height="30" viewBox="0 0 31 30" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <rect x="2.57227" y="2" width="26" height="26" stroke-width="4"/>
                        </svg>
                        @break

                    @case(2)
                        <svg width="31" height="30" viewBox="0 0 31 30" fill="none" xmlns="http://www.w3.org/2000/svg" class="fill">
                            <path d="M15.5725 12.249L3.3235 0L0.572448 2.75105L12.8215 15.0001L0.572266 27.2492L3.32332 30.0003L15.5725 17.7511L27.8216 30.0002L30.5726 27.2491L18.3236 15.0001L30.5724 2.75118L27.8214 0.000123205L15.5725 12.249Z"/>
                        </svg>
                        @break
                
                    @default
                        <svg width="31" height="30" viewBox="0 0 31 30" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <circle cx="15.5723" cy="15" r="13" stroke-width="4"/>
                        </svg>
                @endswitch

            </div>
            <div>
                <svg width="32" height="30" viewBox="0 0 32 30" fill="none" xmlns="http://www.w3.org/2000/svg" class="fill">
                    <path d="M24.0462 13.0715L13.7016 2.72692L16.4285 0L31.4285 15L16.4285 30L13.7016 27.2731L24.0462 16.9285H0.572266V13.0715H24.0462Z"/>
                </svg>                    
            </div>
        </div>
        @endif
    </div>
</div>

@endforeach






<script src="https://unpkg.com/imagesloaded@4/imagesloaded.pkgd.min.js"></script>
<script src="/html2canvas.min.js"></script>
<script>

let images = document.querySelectorAll('.image__wrap');
let results = document.getElementById('resutls');

images.forEach(function(el){

    imagesLoaded( el, function( instance ) {
        html2canvas(el, {
            backgroundColor: null,
            width: el.offsetWidth,
            height: el.offsetHeight,
            scale: 1
        }).then(function(canvas) {
            let img = document.createElement('img');
            img.src = canvas.toDataURL();
            img.classList.add('result');
            results.appendChild(img);
            el.remove();
        });
    });
});
</script>