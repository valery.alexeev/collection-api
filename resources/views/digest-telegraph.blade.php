<style>
li:empty {
    display: none;
}

body {
    background-color: #f6f6f6;
}

#content {
    width: 800px;
    padding: 50px 70px 70px;
    margin: 100px auto;
    background-color: white;
}

.copy {
    position: absolute;
    top: 10px;
    left: 50%;
    width: 150px;
    margin-left: -75px;
}

img {
    max-width: 100%;
    height: auto;
}
</style>

<script>function CopyToClipboard(e){if(document.selection)(t=document.body.createTextRange()).moveToElementText(document.getElementById(e)),t.select().createTextRange(),document.execCommand("copy");else if(window.getSelection){var t;(t=document.createRange()).selectNode(document.getElementById(e)),window.getSelection().addRange(t),document.execCommand("copy"),alert("Text has been copied, now paste in the text-area")}}</script>
<button class="copy" onclick="CopyToClipboard('content')">Скопировать</button>

<div id="content">
<h1>{{ $digest->title }}</h1>
<p>Дизайнерский дайджест от <a href="https://s.va-promotion.ru/webdesigner?utm_source=digest&utm_medium=telegraph&utm_campaign={{ urlencode($digest->title) }}">Школы #VA</a></p>

@if (!empty($digest->content['intro']))
<p>{{ $digest->content['intro'] }}</p>
@endif


@if (!empty($digest->data['tools']))
<br /><hr><br />
<h2>Полезные инструменты</h2>
<p>Подборка приложений и сервисов, которые упростят твою жизнь</p>
    @foreach ($digest->data['tools'] as $item)
    <br />
        @if (!empty($item->image))
            <img src="https://app.5steps.design{{ Storage::url($item->image) }}">
        @endif
        <p>{{ $item->emoji ?? '⚙️' }} <strong><a href="{{ $item->go_link('medium') }}">{{ $item->title_dot }}</a></strong> {{ $item->description }}</p>
    @endforeach
@endif


<br />

<img src="https://telegra.ph/file/fad67e42d623e1e79b843.gif" />
<aside>
    <b>Курс «Веб-дизайнер».</b> Всего за месяц мы рассказываем о самом важном и учим общаться с заказчиками.<br /><a href="https://s.va-promotion.ru/webdesigner?utm_source=digest&utm_medium=telegraph&utm_campaign={{ urlencode($digest->title) }}">Подробнее о курсе →</a></p>
</aside>
<br />
<p>Что самое важное в процессе освоения новой профессии? <b>Быстро получить базовые знания</b> и во время понять, подходит ли тебе новая специализация. Многие курсы веб-дизайна растягиваются на полгода-год, в то время как основные законы и инструменты <b>можно выучить за месяц.</b></p>
<p>Наш онлайн-курс стартует в <b>ближайший понедельник — {{ $digest->data['course_date'] }}.</b> Для всех студентов и школьников мы предоставляем хорошую скидку на освоение этого интересного ремесла. Изучать лекции можно после работы или учёбы — для эффективного обучения достаточно тратить <b>2-3 часа в день</b>. Переходи по ссылке и регистрируйся на курс!</p>


@if (!empty($digest->data['articles']))
<br /><hr><br />
<h2>Статьи и публикации</h2>
<p>Полезные материалы для чтения на русском и английском языке</p>
    @foreach ($digest->data['articles'] as $item)
        <br />
        @if (!empty($item->image))
            <img src="https://app.5steps.design{{ Storage::url($item->image) }}">
        @endif
        <p><strong>{{ $item->title_dot }}</strong> {{ $item->description }} <a href="{{ $item->go_link('medium') }}">Читать статью →</a></p>
    @endforeach
@endif




@if (!empty($digest->data['news']))
<br /><hr><br />
<h2>Новости одной строкой</h2>
<p>Важные новости из мира дизайна, маркетинга и бизнеса</p>
<ol>
    @foreach ($digest->data['news'] as $item)
       <li><a href="{{ $item->go_link('medium') }}"><b>{{ $item->title_dot }}</b></a> {{ $item->description }}</li>
    @endforeach
</ol>
@endif


@if (!empty($digest->data['video']))
<br /><hr><br />
<h2>Видео и лекции</h2>
<p>Познавательные материалы для изучения в свободное время</p>

@foreach ($digest->data['video'] as $item)
    <br />
    <h3>{{ $item->title }}</h3> 
    <p>{{ $item->description }}</p>
    <p>{{ $item->url }}</p>
@endforeach

@endif




@if (!empty($digest->data['inspiration']))
<br /><hr><br />
<h2>Вдохновение и кейсы</h2>
<p>Подборка крутых проектов для поиска новых идей и вдохновения</p>
    @foreach ($digest->data['inspiration'] as $item)
    <br />
        @if (!empty($item->image))
            <img src="https://app.5steps.design{{ Storage::url($item->image) }}">
        @endif
        <p>{{ $item->emoji ?? '🎨' }} <a href="{{ $item->go_link('medium') }}"><strong>{{ $item->title_dot }}</strong></a> {{ $item->description }}</p>
    @endforeach
@endif



@if (!empty($digest->data['other']))
<br /><hr><br />
<h2>Прочие ништяки и интересности</h2>
<p>Всё полезное и интересное, что не удалось разметить в других категориях</p>
<ol>
    @foreach ($digest->data['other'] as $item)
        <li>
            <a href="{{ $item->go_link('medium') }}"><b>{{ $item->title_dot }}</b></a> {{ $item->description }}
        </li>
    @endforeach
</ol>
@endif



<br /><hr><br />

<h2>Заключение</h2>
<p>Обязательно делитесь в комментариях своими мыслями по поводу новостей и материалов за прошедшую неделю. Мне будет полезно узнать ваше мнение.</p>
<p>Не забывайте подписываться на <a href="https://5steps.design/digest-signupj">обновления дайджеста</a>. Я также буду рад видеть вас в своём <a href="https://instagram.com/valery_alexeev">Instagram</a> и на <a href="https://youtube.com/va_promotion">YouTube</a>. Хороших выходных!</p>
</div>
