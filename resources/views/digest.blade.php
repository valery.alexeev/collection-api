@if (!empty($context) && in_array($context, ['email', 'vc', 'web', 'medium', 'telegraph']))
    @include('digest-' . $context, ['digest' => $digest])
@else
    @include('digest-email', ['digest' => $digest])
@endif