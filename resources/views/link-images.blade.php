<link rel="stylesheet" href="/fonts/stylesheet.css?v=12">
<style>
* {
    position: relative;
    padding: 0px;
    margin: 0px;
    box-sizing: border-box;
}

body {
    background-color: #eee;
}

.image {
    font-family: Inter, sans-serif;
}

.image__content {
    position: absolute;
}

.image__wrap--story {
    width: 1080px;
}

.image__wrap--sticker {
    width: 972px;
}

.image__stories {
    width: 1080px;
    height: 1920px;
    background-image: url('/images/story.jpeg');
    background-size: cover;
}

.card_only .image__stories {
    width: auto;
    height: auto;
    background-image: none;
}

.image__stories .image__content {
    text-align: left;
    background-color: white;
    border-radius: 27px;
    width: 90%;
    padding: 80px;
    left: 50%;
    top: 50%;
    transform: translate(-50%, -50%);
}

.card_only .image__stories .image__content {
    top: auto;
    left: auto;
    transform: none;
    width: 972px;
    position: relative;
}

.image__stories .image__content h1 {
    margin: 60px 0px 27px;
    font-size: 54px;
    font-family: "PP Pangram Sans";
    line-height: 1.3;
}

.image__stories .image__content p {
    font-size: 38px;
    line-height: 1.5;
}

.image__stories .image__content p.author {
    color: #444;
    text-transform: uppercase;
    margin-top: 60px;
    font-size: 32px;
    letter-spacing: 0.05em;
}

.image__stories .image__content .emoji__wrap {
    position: absolute;
    width: 172px;
    height: 172px;
    background-color: white;
    border-radius: 50%;
    left: 50%;
    top: -86px;
    margin-left: -86px;
    text-align: center;
    line-height: 172px;
    border: 10px solid #689319;
}

.image__wrap--sticker.without-image .image__stories .image__content .emoji__wrap {
    left: auto;
    margin-left: -25px;
}

.image__stories .image__content .emoji {
    width: 76px;
    margin-top: 40px;
    display: inline-block;
    font-size: 76px;
    line-height: 72px;
    max-width: 76px !important;
    
}

.image__content__cover {
    width: 100%;
    width: calc(100% + 160px);
    border-radius: 27px 27px 0px 0px;
    margin: -80px -80px 110px -80px;
    display: block;
    height: 405px;
    background-size: cover;
    background-color: #eee;
}

.image__content__cover ~ .emoji__wrap {
    top: 324px !important;
    border: 0px !important;
}

.help {
    color: white;
    position: absolute;
    top: 120px;
    text-align: center;
    left: 0;
    width: 100%;
    font-size: 30px;
    opacity: .8;
    text-transform: uppercase;
    letter-spacing: 0.05em;
}

.help__bottom {
    bottom: 162px;
    top: auto;
    font-size: 32px;
    padding: 0px 110px;
    line-height: 1.5;
    text-transform: none;
    letter-spacing: 0px;
}


.result {
    width: 350px;
    height: auto;
    border-radius: 4px;
    border: 4px solid white;
    box-shadow: 0px 0px 0px 1px #eee;
}

.card_only.without-image {
    padding-top: 120px;
}

.card_only.without-image .image__stories .image__content .emoji__wrap {
    position: relative;
    margin-top: -150px;
    top: auto;
    margin-bottom: -20px;
    border-color: white;
    background-color: transparent;
    border: 0px;
    height: 160px;
}

.card_only.without-image .image__stories .image__content .emoji__wrap .emoji {
    margin-top: 0px;
    width: 140px;
    max-width: 140px !important;
    font-size: 140px;
    line-height: 140px;
}


.image__stories .image__content p.author svg {
    width: 50px;
    height: 50px;
    display: inline-block;
    vertical-align: middle;
    margin-right: 8px;
    margin-top: -6px;
}



.url_sticker {
    
    border-radius: 20px;
    padding: 20px 50px;
    text-transform: uppercase;
    color: #ddd;
}

.url_sticker svg {
    width: 40px;
    height: 40px;
    display: inline-block;
    vertical-align: top;
    fill: #ddd;
}

.footer {
    font-size: 32px;
    display: flex;
    justify-content: space-between;
    margin-top: 40px;
    align-items: center;
}

.footer .arrow {
    flex-grow: 1;
    text-align: right;
    margin-left: 40px;
    text-transform: uppercase;
    letter-spacing: 0.2em;
    /* color: #aaa; */
    background-image: url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAfcAAAAmCAMAAADuviIrAAAAJFBMVEUAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAACmWAJHAAAAC3RSTlMAECBQYHCAn8/Q37iXjREAAACBSURBVHja7dexEQIxAANB/dsA2P33CxEJOcOg3RYukfLhmCPUOdfe9/SR/WWGwux7pJDs68wXIDuyIzuyIzuyIzuyIzuyI7vs+3alxzs7VXLZ6E6JHGvTx67r5Mf9Igce4REe4REe4REe4REe4YU/QmP4GRrDj1AY/hHqnGOGf/cEkVJ266Lgl3QAAAAASUVORK5CYII=);
    background-repeat: no-repeat;
    background-size: auto 25px;
    background-position: left center;
}



.footer .arrow span {
    display: inline-block;
    padding-left: 30px;
    background-color: white;
    z-index: 20;
}

</style>


<div class="image__wrap image__wrap--story">
    <div class="image image__stories">
        <div class="image__content">
            @if (!empty($link->image))
                <div style="background-image: url(https://app.5steps.design{{ Storage::url($link->image) }})" class="image__content__cover"></div>
            @endif
            @if (!empty($link->emoji))
            <div class="emoji__wrap">
                {!! emoji($link->emoji ?? ($emoji ?? '📎'), 50) !!}
            </div>
            @endif
            <h1>{{ $link->title }}</h1>
            <p>{{ $link->description }}</p>

            <div class="footer">
                <div class="url_sticker">
                    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" width="24" height="24"><path fill="none" d="M0 0h24v24H0z"/><path d="M17.657 14.828l-1.414-1.414L17.657 12A4 4 0 1 0 12 6.343l-1.414 1.414-1.414-1.414 1.414-1.414a6 6 0 0 1 8.485 8.485l-1.414 1.414zm-2.829 2.829l-1.414 1.414a6 6 0 1 1-8.485-8.485l1.414-1.414 1.414 1.414L6.343 12A4 4 0 1 0 12 17.657l1.414-1.414 1.414 1.414zm0-9.9l1.415 1.415-7.071 7.07-1.415-1.414 7.071-7.07z"/></svg>
                    {{ getDomainFromURL($link->url) }}
                </div>

                <div class="arrow">
                    <span>Клик</span>
                </div>
            </div>

            {{-- <p class="author"><svg enable-background="new 0 0 24 24" height="512" viewBox="0 0 24 24" width="512" xmlns="http://www.w3.org/2000/svg"><path d="m9.417 15.181-.397 5.584c.568 0 .814-.244 1.109-.537l2.663-2.545 5.518 4.041c1.012.564 1.725.267 1.998-.931l3.622-16.972.001-.001c.321-1.496-.541-2.081-1.527-1.714l-21.29 8.151c-1.453.564-1.431 1.374-.247 1.741l5.443 1.693 12.643-7.911c.595-.394 1.136-.176.691.218z" fill="#039be5"/></svg> va_school</p> --}}
        </div>
    </div>
</div>

 
<div class="image__wrap card_only image__wrap--sticker {{ empty($link->image) ? 'without-image' : '' }}">
    <div class="image image__stories">
        <div class="image__content">
            @if (!empty($link->image))
                <div style="background-image: url(https://app.5steps.design{{ Storage::url($link->image) }})" class="image__content__cover"></div>
            @endif
            @if (!empty($link->emoji))
            <div class="emoji__wrap">
                {!! emoji($link->emoji ?? ($emoji ?? '📎'), 50) !!}
            </div>
            @endif
            <h1>{{ $link->title }}</h1>
            <p>{{ $link->description }}</p>

            <div class="footer">
                <div class="url_sticker">
                    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" width="24" height="24"><path fill="none" d="M0 0h24v24H0z"/><path d="M17.657 14.828l-1.414-1.414L17.657 12A4 4 0 1 0 12 6.343l-1.414 1.414-1.414-1.414 1.414-1.414a6 6 0 0 1 8.485 8.485l-1.414 1.414zm-2.829 2.829l-1.414 1.414a6 6 0 1 1-8.485-8.485l1.414-1.414 1.414 1.414L6.343 12A4 4 0 1 0 12 17.657l1.414-1.414 1.414 1.414zm0-9.9l1.415 1.415-7.071 7.07-1.415-1.414 7.071-7.07z"/></svg>
                    {{ getDomainFromURL($link->url) }}
                </div>

                <div class="arrow">
                    <span>Клик</span>
                </div>
            </div>

            {{-- <p class="author"><svg enable-background="new 0 0 24 24" height="512" viewBox="0 0 24 24" width="512" xmlns="http://www.w3.org/2000/svg"><path d="m9.417 15.181-.397 5.584c.568 0 .814-.244 1.109-.537l2.663-2.545 5.518 4.041c1.012.564 1.725.267 1.998-.931l3.622-16.972.001-.001c.321-1.496-.541-2.081-1.527-1.714l-21.29 8.151c-1.453.564-1.431 1.374-.247 1.741l5.443 1.693 12.643-7.911c.595-.394 1.136-.176.691.218z" fill="#039be5"/></svg> va_school</p> --}}
        </div>
    </div>
</div>

<div class="image__wrap card_only image__wrap--sticker without-image">
    <div class="image image__stories">
        <div class="image__content">
            @if (!empty($link->emoji))
            <div class="emoji__wrap">
                {!! emoji($link->emoji ?? ($emoji ?? '📎'), 50) !!}
            </div>
            @endif
            <h1>{{ $link->title }}</h1>
            <p>{{ $link->description }}</p>

            <div class="footer">
                <div class="url_sticker">
                    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" width="24" height="24"><path fill="none" d="M0 0h24v24H0z"/><path d="M17.657 14.828l-1.414-1.414L17.657 12A4 4 0 1 0 12 6.343l-1.414 1.414-1.414-1.414 1.414-1.414a6 6 0 0 1 8.485 8.485l-1.414 1.414zm-2.829 2.829l-1.414 1.414a6 6 0 1 1-8.485-8.485l1.414-1.414 1.414 1.414L6.343 12A4 4 0 1 0 12 17.657l1.414-1.414 1.414 1.414zm0-9.9l1.415 1.415-7.071 7.07-1.415-1.414 7.071-7.07z"/></svg>
                    {{ getDomainFromURL($link->url) }}
                </div>

                <div class="arrow">
                    <span>Клик</span>
                </div>
            </div>

            {{-- <p class="author"><svg enable-background="new 0 0 24 24" height="512" viewBox="0 0 24 24" width="512" xmlns="http://www.w3.org/2000/svg"><path d="m9.417 15.181-.397 5.584c.568 0 .814-.244 1.109-.537l2.663-2.545 5.518 4.041c1.012.564 1.725.267 1.998-.931l3.622-16.972.001-.001c.321-1.496-.541-2.081-1.527-1.714l-21.29 8.151c-1.453.564-1.431 1.374-.247 1.741l5.443 1.693 12.643-7.911c.595-.394 1.136-.176.691.218z" fill="#039be5"/></svg> va_school</p> --}}
        </div>
    </div>
</div>

<script src="https://unpkg.com/imagesloaded@4/imagesloaded.pkgd.min.js"></script>
<script src="/html2canvas.min.js"></script>
<script>

let images = document.querySelectorAll('.image__wrap');

images.forEach(function(el){

    imagesLoaded( el, function( instance ) {
        html2canvas(el, {
            backgroundColor: null,
            width: el.offsetWidth,
            height: el.offsetHeight,
            scale: 1
        }).then(function(canvas) {
            let img = document.createElement('img');
            img.src = canvas.toDataURL();
            img.classList.add('result');
            document.body.appendChild(img);
            el.remove();
        });
    });
});
</script>