<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "https://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="https://www.w3.org/1999/xhtml">
    <head>
        <title>{{ $digest->title }}</title>
        <meta http–equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta http–equiv="X-UA-Compatible" content="IE=edge" />
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <style>
            .link-hover:hover {
				color: #689317 !important;
			}
			@media screen and (max-width: 600px) {
				.row-book {
					display: flex;
					flex-direction: column;
				}
				.row-book > td {
					display:block; 
					box-sizing:border-box; 
					clear:both; 
					order: 2;
				}
				.row-book > td.book-cell {
					order: 1;
					padding-bottom: 30px;
				}
			}
        </style>
</head>

@php
	$color = "#fff";
	function switchColor() {
		global $color;
		$color = $color == '#fff' ? '#F9FAF7' : '#fff';
		return $color;
	}
	function getColorName() {
		global $color;
		return $color == '#fff' ? 'green' : 'white';
	}
@endphp

<body style="margin:0px; padding:0px;" bgcolor="white">
	<p style="font-size: 0px; opacity: 0;">{{ $digest->content['intro'] ?? 'Ещё один дизайнерский дайджест с полезными ссылками, инструментами, кейсами и статьями. Располагайтесь поудобнее, будет интересно!' }}</p>
	<table align="center" width="100%" border="0" cellspacing="0" cellpadding="0" class="main_table" style="font-family: Inter, Arial, sans-serif; width:100%;">

		<tr class="section section--white">
			<td style="background-color: #fff; padding-top: 60px;">
				<table align="center" border="0" cellspacing="0" cellpadding="0" style="width: 90%; max-width: 660px">
					<tr>
                        <td colspan="2" style="padding-bottom: 10px;">
                            <div style="font-size: 64px; padding-bottom: 10px; padding-top: 10px; line-height: 64px;">{!! emoji('🤟🏻', 72) !!}</div>
                            <h1 style="margin-bottom: 0px; margin-top:10px; font-size: 32px;">{{ $digest->title }}</h1>
                            <p style="line-height: 1.5;">Ещё один дизайнерский дайджест с полезными ссылками, инструментами, кейсами и статьями. Располагайтесь поудобнее, будет интересно!</p>
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 72px;"  valign="middle">
							<img src="{{ env('APP_URL') }}/author.png" style="width: 60px; height: auto">
                        </td>
                        <td valign="middle">
                            <p style="margin-top: 0px; margin-bottom: 0px;"><strong>Валерий Алексеев</strong></p>
                            <p style="margin-top: 5px; font-size: 12px; margin-bottom: 0px; opacity: 0.5">UX/UI дизайнер, руководитель Школы #VA</p>
                        </td>
					</tr>
					
					@if (!empty($digest->content['intro']))
					<tr>
						<td colspan="2" style="padding-top: 25px;">
							<p style="background-color: #F9FAF7; border: 1px solid #689317; padding: 28px 35px; border-radius: 8px; margin-top: 0px; margin-bottom: 0px; font-size: 15px; line-height: 1.6;">{{ $digest->content['intro'] }}</p>
						</td>
					</tr>
					@endif
				</table>
			</td>
		</tr>

		@if (!empty($digest->data['tools']))
		<tr class="section section--white">
			<td style="background-color: {{ switchColor() }}; padding-top: 60px; padding-bottom: 60px;">
				<table align="center" border="0" cellspacing="0" cellpadding="0" style="width: 90%; max-width: 660px">
					@include('digest.heading', ['title' => 'Полезные инструменты', 'subtitle' => 'Подборка приложений и сервисов, которые упростят твою жизнь'])
					@foreach ($digest->data['tools'] as $item)
						@include('digest.item', ['item' => $item, 'emoji' => '⚙️'])
					@endforeach
				</table>
			</td>
		</tr>
		@endif


		<tr class="section section--green">
			<td style="background-color: #689317; padding-top: 80px; padding-bottom: 80px;">
				<table align="center" border="0" cellspacing="0" cellpadding="0" style="width: 90%; max-width: 660px; color: white;">

					@include('email.heading', ['title' => 'Изучай веб-дизайн вместе с нами', 'subtitle' => 'Успей записаться на ближайший поток курса «Веб-дизайнер»'])

					<tr>
						<td>
							<div class="text-block" style="line-height: 1.55;">
								<p style="line-height: 1.55;">Что самое важное в&nbsp;процессе освоения новой профессии? <b>Быстро получить базовые знания</b> и&nbsp;во&nbsp;время понять, подходит&nbsp;ли тебе новая специализация. Многие курсы веб-дизайна растягиваются на&nbsp;полгода-год, в&nbsp;то&nbsp;время как основные законы и&nbsp;инструменты <b>можно выучить за&nbsp;месяц</b>.</p>

								<div style="margin: 30px 0px; background-color: white; background-image: url({{ env('APP_URL') }}/banner-course-white.png); background-size: cover; padding: 35px 40px; background-position: left center; border-radius: 8px;">
									<p style="color: black; color: black !important; text-transform: uppercase;font-size: 12px;letter-spacing: 0.1em;margin-top: 0px;margin-bottom: 0px;opacity: .7;">{{ $digest->data['course_date'] }}</p>
									<h2 style="color: black; color: black !important; margin-top: 10px;font-size: 20px;font-weight: 700;margin-bottom: 0px;">Курс «Веб-дизайнер»</h2>
									<p style="color: black; color: black !important; font-size: 15px; max-width: 350px;line-height: 1.55;margin-bottom: 20px;margin-top: 10px;">Всего за&nbsp;месяц мы&nbsp;рассказываем о&nbsp;самом важном и&nbsp;учим общаться с&nbsp;заказчиками.</p>
									<a href="https://s.va-promotion.ru/webdesigner?utm_source=digest&utm_medium=email&utm_campaign={{ urlencode($digest->title) }}&roistat=sendpulse_{{ urlencode($digest->title) }}_webdesigner" target="_blank" style="display: inline-block; background-color: #e4441f; color: white; text-decoration: none; font-size: 14px; padding: 12px 18px; border-radius: 4px;">Подробнее о курсе →</a>
								</div>

								<p style="margin-bottom: 0px; line-height: 1.55;">Наш онлайн-курс стартует <b>в&nbsp;ближайший понедельник</b>. Для всех студентов и&nbsp;школьников мы&nbsp;предоставляем хорошую скидку на&nbsp;освоение этого интересного ремесла. Изучать лекции можно после работы или учёбы&nbsp;&mdash; для эффективного обучения достаточно тратить <b>2-3 часа в&nbsp;день</b>. Переходи по&nbsp;ссылке и&nbsp;регистрируйся на&nbsp;курс!</p>
							</div>
						</td>
					</tr>

				</table>
			</td>
		</tr>

		@if (!empty($digest->data['book']))
		<tr class="section section--white">
			<td style="background-color: white; padding-bottom: 80px;">
				<table align="center" border="0" cellspacing="0" cellpadding="0" style="width: 90%; max-width: 660px">
					@include('digest.book', ['book' => $digest->data['book']])
				</table>
			</td>
		</tr>
		@endif

		@if (!empty($digest->data['articles']))
		<tr class="section section--{{ getColorName() }}">
			<td style="background-color: {{ switchColor() }}; padding-top: 80px; padding-bottom: 80px;">
				<table align="center" border="0" cellspacing="0" cellpadding="0" style="width: 90%; max-width: 660px;">
					@include('digest.heading', ['title' => 'Статьи и публикации', 'subtitle' => 'Полезные материалы для чтения на русском и английском языке'])
					@foreach ($digest->data['articles'] as $item)
						@include('digest.item-article', ['item' => $item, 'emoji' => '🗒'])
					@endforeach
				</table>
			</td>
		</tr>
		@endif


		@if (!empty($digest->data['news']))
		<tr class="section section--{{ getColorName() }}">
			<td style="background-color: {{ switchColor() }}; padding-top: 80px; padding-bottom: 80px;">
				<table align="center" border="0" cellspacing="0" cellpadding="0" style="width: 90%; max-width: 660px;">
					@include('digest.heading', ['title' => 'Новости одной строкой', 'subtitle' => 'Важные новости из мира дизайна, маркетинга и бизнеса'])
					@foreach ($digest->data['news'] as $item)
						@include('digest.item-news', ['item' => $item, 'index' => $loop->iteration])
					@endforeach
				</table>
			</td>
		</tr>
		@endif


		@if (!empty($digest->data['video']))
		<tr class="section section--{{ getColorName() }}">
			<td style="background-color: {{ switchColor() }}; padding-top: 80px; padding-bottom: 80px;">
				<table align="center" border="0" cellspacing="0" cellpadding="0" style="width: 90%; max-width: 660px;">
					@include('digest.heading', ['title' => 'Видео и лекции', 'subtitle' => 'Познавательные материалы для изучения в свободное время'])
					@foreach ($digest->data['video'] as $item)
						@include('digest.item-video', ['item' => $item])
					@endforeach
				</table>
			</td>
		</tr>
		@endif


		@if (!empty($digest->data['inspiration']))
		<tr class="section section--{{ getColorName() }}">
			<td style="background-color: {{ switchColor() }}; padding-top: 80px; padding-bottom: 80px;">
				<table align="center" border="0" cellspacing="0" cellpadding="0" style="width: 90%; max-width: 660px;">
					@include('digest.heading', ['title' => 'Вдохновение и кейсы', 'subtitle' => 'Подборка крутых проектов для поиска новых идей и вдохновения'])
					@foreach ($digest->data['inspiration'] as $item)
						@include('digest.item', ['item' => $item, 'emoji' => '🎨'])
					@endforeach
				</table>
			</td>
		</tr>
		@endif

		@if (!empty($digest->data['other']))
		<tr class="section section--{{ getColorName() }}">
			<td style="background-color: {{ switchColor() }}; padding-top: 80px; padding-bottom: 80px;">
				<table align="center" border="0" cellspacing="0" cellpadding="0" style="width: 90%; max-width: 660px">
					@include('digest.heading', ['title' => 'Прочие ништяки', 'subtitle' => 'Всё полезное и интересное, что не удалось разметить в других категориях'])
					@foreach ($digest->data['other'] as $item)
						@include('digest.item-news', ['item' => $item, 'index' => $loop->iteration])
					@endforeach
				</table>
			</td>
		</tr>
		@endif

        <tr class="footer">
            <td style="background-color: #333; font-size: 14px; padding-top: 35px; padding-bottom: 40px;">
                <table align="center" border="0" cellspacing="0" cellpadding="0" style="width: 90%; max-width: 660px">
                    <tr style="text-align: center;">
                        <td style="color: #ccc; line-height: 1.5">
                            <p style="font-size: 42px; margin-top: 0px; margin-bottom: 8px; line-height: 42px;">{!! emoji('🤔', 42) !!}</p>
                            <p style="font-weight: 700; margin-top: 0px; margin-bottom: 8px; color: white;">Уже уходите..?</p>
                            <p style=" margin-top: 0px; margin-bottom: 16px;">Вы получили это письмо, так как оставили свою почту и согласие на рассылку от Школы #VA</p>
						<p style=" margin-top: 0px; margin-bottom: 0px;"><strong>Хотите получать меньше писем от нас?</strong><br> Вы можете <a style="color: white;" href="@{{unsubscribe_url}}">отписаться</a> в один клик.</p>
                        </td>
                    </tr>
				</table>
            </td>
        </tr>

	</table>

	<link href="https://fonts.googleapis.com/css2?family=Inter:wght@400;700&display=swap" rel="stylesheet">
</body>
</html>