<?php

function emoji($emoji, $width = '100%') {
    $url = getEmojiFileUrl($emoji);

    if (file_exists(public_path($url))) {
        return ('<img src="' . env('APP_URL') . $url . '" class="emoji" style="max-width: ' . (is_integer($width) ? $width . 'px' : $width) . '; height: auto;" />');
    } else {

        // Log missing emojis
        $missing_path = public_path('emoji/missing.json');
        $missing_emojis = json_decode(file_get_contents($missing_path), true);
        if (!isset($missing_emojis[$emoji])) {
            $missing_emojis[$emoji] = $url;
            file_put_contents($missing_path, json_encode($missing_emojis));
        }

        return ('<span class="emoji" data-missing="' . $url . '">' . $emoji . '</span>');
    }
}


function getEmojiFileUrl($emoji) {
    $converter = new \Chefkoch\Morphoji\Converter();
    $code = $converter->fromEmojis($emoji);
    $code = str_replace('::', '-', $code);
    $code = str_replace([':', 'emoji-'], '', $code);
    $url = '/emoji/' . $code . '.png';
    return $url;
}


function getDomainFromURL($url) {
    $parse = parse_url($url);
    $domain = $parse['host'];

    if (strpos($domain, 'www.') === 0) {
        $domain = str_replace('www.', '', $domain);
    }

    return $domain;
}


function styleTextBlockForEmail($text) {

    $text = preg_replace_callback('/<blockquote.*?>(.*?)<\/blockquote>/s', function($matches){
        return '<blockquote>' . strip_tags($matches[1]) . '</blockquote>';
    }, $text);

    $text = str_replace(

        [
            '<p>',
            '<blockquote>',
            '<img ',
            '<li>',
            '<hr />'
        ], 
        
        [
            '<p style="font-size: 16px; line-height: 1.65; margin-top: 15px; margin-bottom: 15px; font-family: Inter, Arial, sans-serif;">',
            '<blockquote style="border-left: 4px solid #689317; font-size: 18px; padding-left: 20px; padding-top: 10px; padding-bottom: 10px; font-family: Inter, Arial, sans-serif;">',
            '<img style="width: 100%; height: auto; display: block; margin: 30px auto; border-radius: 8px;" ',
            '<li style="font-size: 16px; line-height: 1.55; margin-top: 10px; margin-bottom: 10px;">',
            '<div style="height: 1px; background-color: #ddd; margin: 40px 0px; padding: 0px; font-size: 0px;"></div>'
        ], 
        
        $text);


    return $text;
}



function typograf ($input) {
    $t = new \Akh\Typograf\Typograf();
    return $t->apply($input);
}