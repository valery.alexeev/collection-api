<?php

namespace App\Observers;

use App\Models\Link;
use Illuminate\Support\Facades\Log;

class LinkObserver
{
    /**
     * Handle the link "created" event.
     *
     * @param  \App\Models\Link  $link
     * @return void
     */
    public function created(Link $link)
    {
        if (empty($link->image)) {
            try {
                $link->saveLinkImage();
            } catch (\Throwable $th) {
                Log::emergency($th->getMessage());
            }
        }
        try {
            $link->scheduleNewTelegramPost();
            Link::scheduleTelegramPost();
        } catch (\Exception $e) {
            Log::error("Error while scheduling for telegram:" . $e->getMessage());
        }
    }

    /**
     * Handle the link "updated" event.
     *
     * @param  \App\Models\Link  $link
     * @return void
     */
    public function updated(Link $link)
    {
        //
    }

    /**
     * Handle the link "deleted" event.
     *
     * @param  \App\Models\Link  $link
     * @return void
     */
    public function deleted(Link $link)
    {
        //
    }

    /**
     * Handle the link "restored" event.
     *
     * @param  \App\Models\Link  $link
     * @return void
     */
    public function restored(Link $link)
    {
        //
    }

    /**
     * Handle the link "force deleted" event.
     *
     * @param  \App\Models\Link  $link
     * @return void
     */
    public function forceDeleted(Link $link)
    {
        //
    }
}
