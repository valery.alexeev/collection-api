<?php

namespace App\Http\Resources;

use Illuminate\Support\Facades\Storage;
use Illuminate\Http\Resources\Json\JsonResource;

class Link extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'id'            => $this->id,
            'title'         => $this->title,
            'description'   => $this->description,
            'favicon'       => $this->favicon,
            'domain'        => $this->domain,
            'url'           => $this->url,
            'emoji'         => $this->emoji,
            'emoji_url'     => $this->emojiUrl(),
            'image'         => Storage::url($this->image),
            'extras'        => $this->extras,
            'in_favourite'  => $this->when((request()->has('email') || request()->cookie('email')) && $this->relationLoaded('favourites'), $this->in_favourites)
        ];
    }


    public function emojiUrl()
    {
        $url = getEmojiFileUrl($this->emoji);
        if (file_exists(public_path($url))) {
            return $url;
        } else {
            return false;
        }
    }
}
