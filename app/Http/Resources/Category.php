<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class Category extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'id'            => $this->id,
            'depth'         => $this->depth,
            'parent_id'     => $this->parent_id,
            'root_id'       => $this->when($this->relationLoaded('ancestors'), $this->root->id),
            'links_count'   => $this->links_count ?? null,
            'title'         => $this->title,
            'emoji'         => $this->emoji,
            'emoji_url'     => $this->emojiUrl,
        ];


        return collect(parent::toArray($request))->only(['id', 'title', 'links_count', 'parent_id', 'emoji', 'emoji_url']);
    }
}
