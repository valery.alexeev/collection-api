<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use App\Models\Link;
use App\Models\Digest;
use App\Models\Category;
use Illuminate\Http\Request;

class LinkController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request, Category $category)
    {
        // Link::scheduleTelegramPost();
    }



    public function sh_index(Request $request) {
        if (empty($request->filter)) {
            $links = Link::all();
        } else {

            switch ($request->filter) {
                case 'only_drafts':
                    
                    $links = Link::where('status', 'draft')->get()->mapWithKeys(function($item) {
                        return [$item->title => $item->id];
                    });
                    break;
                
                default:
                    # code...
                    break;
            }
        }

        return $links;
    }


    public function sh_destroy(Link $link)
    {
        return $link->delete();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request, Category $category)
    {

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Category $category)
    {
        $request->validate([
            'title' => 'required',
            'description' => 'required',
            'url' => 'required|url'
        ]);

        return $category->links()->create($request->only([
            'title', 'description', 'url', 'emoji',
        ]));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Category $category, Link $link)
    {
        return $link;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Category $category, Link $link)
    {
        return $link->delete();
    }



    public function sh_store(Request $request) {
        $categories = Category::defaultOrder()->withDepth()->get();
        $stripped_name = trim(str_replace(['→', '&nbsp;', ' '], ['', ' ', ' '], $request->category_name));

        $request->validate([
            'title' => 'required',
            'url' => 'required|url'
        ]);

        $digest = Digest::where('status', Digest::STATUS_DRAFT)->first();
        if (empty($digest)) {
            $digest = Digest::create([
                'title' => 'Валера нагуглил (draft) / ' . Carbon::now()->format('d.m.Y'),
                'status' => Digest::STATUS_DRAFT
            ]);
        }

        $chosen_category = $categories->where('title', $stripped_name)->first();
        if (!empty($chosen_category)) {
            $link = $chosen_category->links()->create($request->only([
                'title', 'description', 'url', 'emoji',
            ]));

            if (!empty($link)) {
                $digest->links()->save($link);
                $digest_data = $digest->data;
                $return = "Ссылка <strong>«" . $link->title . "»</strong> опубликована<br><br>";

                foreach ($digest_data as $key => $d) {
                    if (!array_key_exists($key, Digest::DATA_LIMITS)) {
                        continue;
                    }

                    $return .= Digest::DATA_LABELS[$key] . ": " . count(is_array($d) ? $d : []) . " из " . Digest::DATA_LIMITS[$key] . "<br>";
                }

                // $return .= "Открыть "

                echo(json_encode(['status' => $return], JSON_UNESCAPED_UNICODE));
            }
            
        } else {
            throw abort(404, 'Category not exists');
        }
    }

    public function images(Request $request, Link $link) {
        return view('link-images', ['link' => $link]);
    }

    public function go(Request $request, Link $link) {
        $link->clicked($request);
        return redirect($link->url_out);
    }


    public function telegram_publish(Request $request, Link $link) {
        $link->scheduleNewTelegramPost('public');
    }
}
