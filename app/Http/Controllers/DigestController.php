<?php

namespace App\Http\Controllers;

use App\Models\Link;
use App\Models\Digest;
use App\Models\Category;
use Illuminate\Http\Request;

class DigestController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //Link::find(101)->getLinkImage();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        // $links = Link::where('status', 'draft')->with('category.ancestors')->orderBy('category_id')->get();
        // dd($links->pluck('nested_title_chain'));

        dd(Category::find(6)->nested_title_chain);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Digest  $digest
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, Digest $digest)
    {
        return view('digest', ['digest' => $digest, 'context' => $request->context ?? 'email']);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Digest  $digest
     * @return \Illuminate\Http\Response
     */
    public function edit(Digest $digest)
    {
        //
    }

    public function cover(Request $request, Digest $digest) {
        $links = $digest->links()->whereNotNull('image')->inRandomOrder()->limit(6)->get();
        return view('digest-cover', ['digest' => $digest, 'images' => $links->pluck('image')]);
    }

    public function stat(Request $request, Digest $digest) {
        $clicks = $digest->clicks->groupBy('link_id');
        $links = $digest->links;

        $stat = [];
        foreach ($links as $l) {
            $stat[$l->id] = (object) [
                'model' => $l,
                'clicks' => $clicks[$l->id] ?? [],
                'clicks_count' => !empty($clicks[$l->id]) ? count($clicks[$l->id]) : 0,
                'refs' => !empty($clicks[$l->id]) ? $clicks[$l->id]->countBy(function($item){
                    return $item->data->ref ?? 'Не определено';
                }) : []
            ];
        }

        return view('digest.stat', ['stat' => $stat]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Digest  $digest
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Digest $digest)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Digest  $digest
     * @return \Illuminate\Http\Response
     */
    public function destroy(Digest $digest)
    {
        //
    }
}
