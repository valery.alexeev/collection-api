<?php

namespace App\Http\Controllers;

use App\Models\Set;
use App\Models\Link;
use App\Models\Category;
use App\Models\Favourite;
use Illuminate\Http\Request;
use App\Http\Resources\LinksCollection;
use App\Http\Resources\Link as ResourcesLink;
use App\Http\Resources\Category as ResourcesCategory;

class CollectionApiController extends Controller
{

    /**
     * Навигация для коллекции
     *
     * @param Request $request
     * @return void
     */
    public function navigation(Request $request)
    {
        return Category::defaultOrder()->with('ancestors')->forCollection()->withDepth()->withNestedLinksCount()->get()->toFlatTree()->map(function ($category) {
            return new ResourcesCategory($category);
        });
    }


    public function category(Request $request, Category $category)
    {
        $category->load('ancestors');
        return new ResourcesCategory($category);
    }


    public function links(Request $request, Category $category)
    {
        $nestedCategpories = $category->descendants()->pluck('id');
        $nestedCategpories[] = $category->getKey();

        return ResourcesLink::collection(Link::whereIn('category_id', $nestedCategpories)->with('favourites')->orderBy('id', 'desc')->paginate(11));
    }

    public function latest(Request $request)
    {
        return Link::orderBy('id', 'desc')->limit(5)->get();
    }



    public function addToFavourites(Request $request, Link $link)
    {
        $request->validate([
            'email' => 'required'
        ]);

        if (!$link->isInFavourites($request->email)) {
            $link->addToFavourites($request->email);
        }
    }


    public function removeFromFavourites(Request $request, Link $link)
    {
        $request->validate([
            'email' => 'required'
        ]);

        if ($link->isInFavourites($request->email)) {
            $link->removeFromFavourites($request->email);
        }
    }


    public function getAllFavourites(Request $request)
    {
        $request->validate([
            'email' => 'required'
        ]);

        return ResourcesLink::collection(Link::whereIn('id', Favourite::select('link_id')->where('email', $request->email))->with('favourites')->paginate(12));
    }


    public function setLinks(Request $request, Set $set)
    {
        return ResourcesLink::collection($set->links);
    }
}
