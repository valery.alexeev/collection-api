<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Email extends Model
{
    use HasFactory;

    public $fillable = ['title'];

    public $attributes = [
        'content' => '{}'
    ];

    public $casts = [
        'content' => 'array'
    ];

    public function view() {
        return view('email', ['email' => $this, 'blocks' => $this->content['blocks']]);
    }
}
