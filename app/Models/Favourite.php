<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Favourite extends Model
{
    use HasFactory;

    public $fillable = ['email', 'link_id'];

    public function links() {
        return $this->hasMany(Link::class);
    }
}
