<?php

namespace App\Models;

use PDO;
use Kalnoy\Nestedset\NodeTrait;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Novius\LaravelNovaOrderNestedsetField\Traits\Orderable;

class Category extends Model
{
    use HasFactory;
    use NodeTrait;
    use Orderable;

    public $append = ['nested_title'];

    /**
     * Scope a query to only include popular users.
     */
    public function scopeForCollection(Builder $query): void
    {
        $query->whereNotIn('id', [13, 12, 10, 9]);
    }


    /**
     * Scope a query to only include popular users.
     */
    public function scopeWithNestedLinksCount(Builder $query): void
    {
        $query->selectRaw('categories.*, categories._lft as lft, categories._rgt as rgt');

        $query->addSelect(
            [
                'links_count' => function ($builder) {
                    $builder->selectRaw("count(*)")
                        ->from('links')
                        ->join('categories as link_category', 'link_category.id', '=', 'links.category_id')
                        ->where(function ($q) {
                            $q->whereRaw("`categories`.`id` = `links`.`category_id`");
                            $q->orWhereRaw("link_category._lft > lft AND link_category._lft < rgt");
                        });
                }
            ]
        );
    }

    public function links()
    {
        return $this->hasMany('App\Models\Link');
    }


    public function getNestedTitleAttribute()
    {
        return str_repeat('→ ', $this->depth) . $this->title;
    }

    public function getNestedTitleChainAttribute()
    {
        $result = '';
        foreach ($this->ancestors as $a) {
            $result .= $a->title . ' → ';
        }

        $result .= $this->title;
        return $result;
    }

    public function getRootAttribute()
    {
        if ($this->parent_id == 0) {
            return $this;
        } else {
            return $this->ancestors->where('parent_id', 0)->first();
        }
    }


    public function getEmojiAttribute()
    {
        switch ($this->id) {
            case 6:
                return "⚙️";
                break;

            case 11:
                return "🖼️";
                break;

            case 3:
                return "📗";
                break;

            case 4:
                return "👨🏻‍🎓";
                break;

            case 5:
                return "🔎";
                break;

            case 8:
                return "▶️";
                break;

            default:
                return false;
                break;
        }
    }


    public function getEmojiUrlAttribute()
    {
        if ($this->emoji) {
            $url = getEmojiFileUrl($this->emoji);
            if (file_exists(public_path($url))) {
                return $url;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }
}
