<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Click extends Model
{
    use HasFactory;

    protected $fillable = ['link_id', 'data', 'context'];

    protected $casts = [
        'data' => 'object'
    ];

    protected $attributes = [
        'data' =>  '{}'
    ];

    public function link() {
        return $this->belongsTo('App\Models\Link');
    }
}
