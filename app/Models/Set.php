<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Set extends Model
{
    use HasFactory;

    public $attributes = [
        'extras' => '{}'
    ];

    public $casts = [
        'extras' => 'object'
    ];

    /**
     * Get all links, this set attached to
     *
     * @return void
     */
    public function links() {
        return $this->belongsToMany('App\Models\Link');
    }
}
