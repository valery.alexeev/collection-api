<?php

namespace App\Models;

use Carbon\Carbon;
use Sendpulse\RestApi\ApiClient;
use Illuminate\Database\Eloquent\Model;
use Sendpulse\RestApi\Storage\FileStorage;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Digest extends Model
{
    use HasFactory;

    const STATUS_DRAFT = 'draft';
    const STATUS_PUBLISHED = 'published';

    const DATA_ARTICLES = 'articles';
    const DATA_VIDEO = 'video';
    const DATA_TOOLS = 'tools';
    const DATA_INSPIRATION = 'inspiration';
    const DATA_NEWS = 'news';
    const DATA_OTHER = 'other';
    const DATA_BOOK = 'book';

    const DATA_LIMITS = [
        self::DATA_TOOLS        => 8,
        self::DATA_ARTICLES     => 5,
        self::DATA_VIDEO        => 3,
        self::DATA_NEWS         => 10,
        self::DATA_BOOK         => 1,
        self::DATA_INSPIRATION  => 5,
        self::DATA_OTHER        => 5
    ];

    const DATA_LABELS = [
        self::DATA_TOOLS        => "Инструменты",
        self::DATA_ARTICLES     => "Статьи",
        self::DATA_VIDEO        => "Видео",
        self::DATA_NEWS         => "Новости",
        self::DATA_BOOK         => "Книги",
        self::DATA_INSPIRATION  => "Вдохновение",
        self::DATA_OTHER        => "Прочее"
    ];

    const DATA_EMOJIS = [
        self::DATA_TOOLS        => "⚙️",
        self::DATA_ARTICLES     => "✏️",
        self::DATA_VIDEO        => "🎬",
        self::DATA_NEWS         => "📌",
        self::DATA_BOOK         => "📗",
        self::DATA_INSPIRATION  => "🔥",
        self::DATA_OTHER        => "🔗"
    ];

    public $attributes = [
        'content' => '{}'
    ];

    public $casts = [
        'content' => 'array'
    ];

    public $fillable = [
        'status', 'title'
    ];

    public function links() {
        return $this->hasMany('App\Models\Link');
    }

    public function sendToSendpulse() {
        $sp_client = new ApiClient(env('SENDPULSE_ID', 'd740fe5b8771c605d03e479b15255e9f'), env('SENDPULSE_SECRET', 'b06d4173df54d0b8ef7ec2dfe3b61f1d'), new FileStorage());

        return $sp_client->createCampaign(
            'Валера из #VA', 
            'va@va-promotion.ru',
            $this->title,
            view('digest', ['digest' => $this])->render(),
            4492,
            $this->title,
            null,
            'draft'
        );
    }


    public function getDataAttribute() {
        $items = $this->links()->with('category.ancestors')->get();
        $data = [
            self::DATA_ARTICLES     => [],
            self::DATA_NEWS         => [],
            self::DATA_TOOLS        => [],
            self::DATA_VIDEO        => [],
            self::DATA_OTHER        => [],
            self::DATA_INSPIRATION  => []
        ];


        // Add book
        if (!empty($this->content['book'])) {
            $data[self::DATA_BOOK] = Link::find($this->content['book']);
        }

        // Add next course date
        $next_monday = new Carbon('next monday');
        $months = ['', 'января', 'февраля', 'марта', 'апреля', 'мая', 'июня', 'июля', 'августа', 'сентября', 'октября', 'ноября', 'декабря'];
        $data['course_date'] = $next_monday->day . ' ' . $months[$next_monday->month];

        foreach ($items as $link) {
            $root = $link->category->ancestors->isNotEmpty() ? $link->category->ancestors->first() : $link->category;

            // IF books
            if ($root->id == 3) {
                continue;
            }

            // dump($link->category->ancestors->isEmpty());

            switch ($root->id) {
                case 1:
                    $key = self::DATA_TOOLS;
                    break;

                case 9:
                    $key = self::DATA_NEWS;
                    break;

                case 8:
                    $key = self::DATA_VIDEO;
                    break;

                case 5:
                    $key = self::DATA_ARTICLES;
                    break;

                case 10:
                    $key = self::DATA_INSPIRATION;
                    break;

                default:
                    $key = self::DATA_OTHER;
                    break;
            }

            $data[$key][] = $link;
        }

        return $data;
    }


    public function clicks() {
        return $this->hasManyThrough('App\Models\Click', 'App\Models\Link');
    }
}
