<?php

namespace App\Models;

use App\Http\Resources\Link as ResourcesLink;
use GuzzleHttp\Client;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use Fusonic\OpenGraph\Consumer;
use Illuminate\Support\Facades\Log;
use Intervention\Image\Facades\Image;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;
use Symfony\Component\HttpClient\Psr18Client;
use Symfony\Component\HttpClient\NativeHttpClient;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Link extends Model
{
    use HasFactory;

    const STATUS_DRAFT = 'draft';
    const STATUS_PUBLISHED = 'published';

    public $attributes = [
        'status' => 'draft',
        'extras' => '{}'
    ];

    public $casts = [
        'extras' => 'object'
    ];

    public $fillable = [
        'title', 'url', 'description', 'emoji', 'status', 'image'
    ];

    public $dates = ['published_at', 'created_at', 'updated_at'];

    public function category()
    {
        return $this->belongsTo('App\Models\Category');
    }

    public function getDomainAttribute()
    {
        return self::extractDomain($this->url);
    }

    public static function extractDomain($url)
    {
        $url = parse_url($url);
        $host = $url['host'];
        return strpos($host, 'www.') === 0 ? substr($host, 4) : $host;
    }

    public function getTitleDotAttribute()
    {
        return self::generateDotTitle($this->title);
    }

    public function digest()
    {
        return $this->belongsTo('App\Models\Digest');
    }

    /**
     * Get all sets, this link attached to
     *
     * @return void
     */
    public function sets()
    {
        return $this->belongsToMany('App\Models\Set');
    }


    public function getShortDescriptionAttribute()
    {
        return Str::limit($this->description, 120, '...');
    }


    public function getLinkImage()
    {
        $regexes = [
            'youtube' =>  '/(?:youtube\.com\/(?:[^\/]+\/.+\/|(?:v|e(?:mbed)?)\/|.*[?&]v=)|youtu\.be\/)([^"&?\/\s]{11})/i',
            'vimeo' => '/(?:www\.|player\.)?vimeo.com\/(?:channels\/(?:\w+\/)?|groups\/(?:[^\/]*)\/videos\/|album\/(?:\d+)\/video\/|video\/|)(\d+)(?:[a-zA-Z0-9_\-]+)?/i',
            'instagram' => '/https:\/\/(www.)?instagram\.com\/p\/(.*)\/.*/i',

            'osnova' => '/(vc\.ru|dtf\/.ru|tjournal\.ru)/i'
        ];
        $url = null;

        // If link is on Vimeo
        if (preg_match($regexes['vimeo'], $this->url, $matches)) {
            $info = json_decode(file_get_contents("https://vimeo.com/api/v2/video/" . $matches[1] . ".json"), true);
            if (!empty($info)) {
                $info = $info[0];
                $url = $info['thumbnail_large'];
            }
        }

        // If link is on YouTube
        elseif (preg_match($regexes['youtube'], $this->url, $matches)) {
            $url = 'https://img.youtube.com/vi/' . $matches[1] . '/maxresdefault.jpg';
        }

        // If link from VC
        elseif (preg_match($regexes['osnova'], $this->url, $matches)) {
            $api_url = "https://api." . $matches[1] . "/v1.8/entry/locate?url=" . $this->url;
            $result = json_decode(file_get_contents($api_url), TRUE);
            Log::info($result);

            if (!empty($result) && !empty($result['result']) && !empty($result['result']['cover'])) {
                $url = $result['result']['cover']['url'] . 'file.' . $result['result']['cover']['additionalData']['type'];
            }
        }


        if (!$url) {
            // If everything else, try to get Meta OG Image
            $client = new Psr18Client(new NativeHttpClient(["headers" => ["User-Agent" => "facebookexternalhit/1.1"]]));

            $consumer = new Consumer($client, $client);
            $object = $consumer->loadUrl($this->url);
            $image = $object->images[0] ?? null;
            if (!empty($image)) {
                $url = $image->url;

                $parsedURL = parse_url($url);
                // Check if the URL is relative
                if (!isset($parsedURL['host'])) {
                    $baseParsedURL = parse_url($this->url);
                    $domain = $baseParsedURL['scheme'] . '://' . $baseParsedURL['host'];
                    $url = $domain . '/' . ltrim($url, '/');
                }
            }
        }

        return $url;
    }



    public function saveLinkImage()
    {
        $url = $this->getLinkImage();

        // If link exists
        if (!empty($url)) {
            try {
                $image = file_get_contents($url);
                $basename = basename($url);

                file_put_contents('/tmp/' . $basename, $image);
                $file = Storage::putFile('/public/videos', '/tmp/' . $basename);
                unlink('/tmp/' . $basename);

                $this->update(['image' => str_replace('public/', '', $file)]);
            } catch (\Throwable $th) {
                throw $th;
            }
        }
    }

    public function clicks()
    {
        return $this->hasMany('App\Models\Click');
    }

    public function getUrlAttribute($url)
    {
        if (empty($url)) {
            return $url;
        }
        $url = \Spatie\Url\Url::fromString($url);
        return (string) $url->withoutQueryParameter('from')->withoutQueryParameter('ref');
    }

    public function getUrlOutAttribute()
    {
        $url = \Spatie\Url\Url::fromString($this->url);
        return (string) $url->withoutQueryParameter('from')->withoutQueryParameter('ref')->withQueryParameter('ref', '5steps.design');
    }

    public function go_link($context = null)
    {
        return route('link.go', [$this, 'context' => $context ?? null]);
    }

    public function clicked(Request $request)
    {
        $this->clicks()->create([
            'context' => $request->context ?? null,
            'data' => [
                'ref' => $request->headers->get('referer')
            ]
        ]);
    }


    public static function generateDotTitle($title)
    {
        $last = substr(trim($title), -1);
        if (in_array($last, ['.', '?', '!', '"', '»'])) {
            return $title;
        } else {
            return $title . '.';
        }
    }




    public function getCategorySlugAttribute()
    {
        $key = null;
        $root = $this->category->ancestors->isNotEmpty() ? $this->category->ancestors->first() : $this->category;

        switch ($root->id) {
            case 3:
                $key = Digest::DATA_BOOK;
                break;
            case 1:
                $key = Digest::DATA_TOOLS;
                break;

            case 9:
                $key = Digest::DATA_NEWS;
                break;

            case 8:
                $key = Digest::DATA_VIDEO;
                break;

            case 5:
                $key = Digest::DATA_ARTICLES;
                break;

            case 10:
                $key = Digest::DATA_INSPIRATION;
                break;

            default:
                $key = Digest::DATA_OTHER;
                break;
        }

        return $key;
    }


    public function getCategoryEmojiAttribute()
    {
        return Digest::DATA_EMOJIS[$this->category_slug] ?? '🔗';
    }



    /**
     * Schedule telegram post
     *
     * @return void
     */
    public static function scheduleTelegramPost()
    {
        $count = Link::count();
        if ($count % 5 === 0) {
            $links = Link::orderBy('id', 'desc')->with('category')->take(5)->get()->sortBy('category_emoji');
            $text = self::scheduleTelegramPostGenerateMessage($links);

            $token = '513260579:AAGnaV-InLUZ0JfKnAJZBlJZKIzvv-MXXX8';
            $client = new Client();

            $response = $client->post('https://api.telegram.org/bot' . $token . '/sendMessage', [
                'json' => [
                    'chat_id' => -1001444757228,
                    'text' => $text,
                    'parse_mode' => 'html',
                    'disable_web_page_preview' => true
                ]
            ]);
        }
    }


    public function scheduleNewTelegramPost($scope = 'admin')
    {
        $text = $this->emoji . " <a href='" . $this->url . "'>{$this->title_dot}</a> ";
        $text .= !empty($this->image) ? '' : "\n\n";
        $text .= $this->description;

        $token = '513260579:AAGnaV-InLUZ0JfKnAJZBlJZKIzvv-MXXX8';
        $client = new Client();

        $data = [
            'json' => [
                'chat_id' => $scope == 'public' ? -1001947591527 : -1001444757228,
                'text' => $text,
                'caption' => $text,
                'photo' => 'https://app.5steps.design' . Storage::url($this->image),
                'parse_mode' => 'html',
                'disable_web_page_preview' => true
            ]
        ];


        if ($scope == 'admin') {
            $data['json']['reply_markup'] = json_encode([
                'inline_keyboard' => [
                    [
                        [
                            'text' => 'Редактировать',
                            'url' => "https://app.5steps.design/nova/resources/links/{$this->id}/edit"
                        ]
                    ],
                    [
                        [
                            'text' => 'Опубликовать',
                            'url' => "https://app.5steps.design/telegram-publish/{$this->id}"
                        ]
                    ],
                    [
                        [
                            'text' => 'Картинки для Instagram',
                            'url' => "https://app.5steps.design/link/{$this->id}/images"
                        ]
                    ]
                ]
            ], JSON_UNESCAPED_UNICODE);
        }
        //  else {
        //     $data['json']['message_thread_id'] = 4;
        // }

        Log::info(print_r($data, true));

        if (!empty($this->image)) {
            $response = $client->post('https://api.telegram.org/bot' . $token . '/sendPhoto', $data);
        } else {
            $response = $client->post('https://api.telegram.org/bot' . $token . '/sendMessage', $data);
        }
        Log::info($response->getBody());
    }




    /**
     * Generate text for telegram post
     *
     * @param [type] $links
     * @return void
     */
    private static function scheduleTelegramPostGenerateMessage($links)
    {
        $text = "<b>Очередная подборка полезных инструментов, ссылок и новостей для дизайнеров</b>\n\n";

        foreach ($links as $l) {
            $text .= $l->category_emoji . " <a href='" . $l->go_link('telegram') . "'>{$l->title_dot}</a> {$l->description}\n\n";
        }

        $text .= "- - - - - - - - - - - - - - -\n\n<i>Не забывайте подписываться на <a href='https://s.va-promotion.ru/digest/?utm_source=telegram&utm_medium=digest&utm_campaign=%D0%A0%D0%B5%D0%B3%D1%83%D0%BB%D1%8F%D1%80%D0%BD%D1%8B%D0%B9%20%D0%B4%D0%B0%D0%B9%D0%B4%D0%B6%D0%B5%D1%81%D1%82%20%D0%B2%20TG'>Email-версию</a> дизайнерского дайджеста, а также заглядывайте ко мне в <a href='https://instagram.com/valery_alexeev'>Instagram</a>, там всегда интересно.</i>";

        return $text;
    }


    private static function scheduleTelegramPostGenerateCover($links)
    {
        $pattern = Image::make(public_path('cover-pattern@2x.png'))->fit(1200, 600);
        $img = Image::canvas(1200, 600, '#659D34')->insert($pattern);

        $emojis = collect([]);
        foreach ($links as $l) {
            $emoji_url = getEmojiFileUrl($l->emoji);
            if (file_exists(public_path($emoji_url))) {
                $emojis->push(public_path($emoji_url));
            }
        }


        foreach ($emojis->random(3) as $i => $e) {
            $e_img = Image::make($e)->resize(180, 180);
            $img->insert($e_img, 'center-left', 230 + ($i * 280));
        }

        $name = '/telegram/' . time() . '.png';
        $path = public_path($name);
        $img->save($path);
        return $name;
    }

    public function isInFavourites($email) {
        return Favourite::where(['email' => $email, 'link_id' => $this->id])->exists();
    }

    public function addToFavourites($email) {
        return Favourite::create(['email' => $email, 'link_id' => $this->id]);
    }

    public function removeFromFavourites($email) {
        return Favourite::where([['email', '=', $email], ['link_id', '=', $this->id]])->delete();
    }

    public function favourites() {
        return $this->hasMany(Favourite::class);
    }

    public function getInFavouritesAttribute() {
        if (request()->has('email') || request()->cookie('email')) {
            return $this->isInFavourites(request()->email ?? request()->cookie('email'));
        } else {
            return null;
        }
    }
}
