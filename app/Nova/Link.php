<?php

namespace App\Nova;

use Illuminate\Support\Str;
use Laravel\Nova\Fields\ID;
use Illuminate\Http\Request;
use Laravel\Nova\Fields\Line;
use Laravel\Nova\Fields\Text;
use Laravel\Nova\Fields\Stack;
use Laravel\Nova\Fields\Select;
use Wehaa\Liveupdate\Liveupdate;
use App\Models\Link as LinkModel;
use Laravel\Nova\Fields\Textarea;
use Laravel\Nova\Fields\BelongsTo;
use Laravel\Nova\Fields\BooleanGroup;
use Laravel\Nova\Fields\BelongsToMany;
use Laravel\Nova\Http\Requests\NovaRequest;
use Ctessier\NovaAdvancedImageField\AdvancedImage;
use Marshmallow\CharcountedFields\TextareaCounted;

class Link extends Resource
{
    /**
     * The model the resource corresponds to.
     *
     * @var string
     */
    public static $model = \App\Models\Link::class;

    /**
     * The single value that should be used to represent the resource when being displayed.
     *
     * @var string
     */
    public static $title = 'title';

    /**
     * The columns that should be searched.
     *
     * @var array
     */
    public static $search = [
        'id', 'title', 'description'
    ];

    /**
     * Get the fields displayed by the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function fields(Request $request)
    {

        $fields = [
            ID::make(__('ID'), 'id')->sortable(),
            Liveupdate::make('Emoji'),

            Stack::make('Ресурс', [
                Line::make('Title'),
                Line::make('Description', 'short_description')->asSmall(),
            ]),

            Text::make('Title')->required()->displayUsing(function ($text) {
                return Str::limit($text, 45);
            })->hideFromIndex(),
            TextareaCounted::make('Description')->minChars(90)->maxChars(180)->warningAt(220)->required(),
            Text::make('URL')->required()->hideFromIndex(),
            // Text::make('Status'),

            Select::make('Категория', 'category_id')
                ->options(function () {
                    return \App\Models\Category
                        ::withDepth()
                        ->defaultOrder()
                        ->get()
                        ->reduce(function ($options, $model) {
                            $options[$model['id']] = $model->nested_title;
                            return $options;
                        }, []);
                })
                ->nullable()
                ->onlyOnForms()
                ->required(),


            // BelongsTo::make('Digest', 'digest', 'App\Nova\Digest'),
            BelongsToMany::make('Sets'),

            AdvancedImage::make('Image')->croppable()->resize(900)->hideFromIndex()
        ];

        if (!empty($request->resourceId)) {
            $link = LinkModel::where('id', $request->resourceId)->with('category.ancestors')->first();
            $root = $link->category->ancestors->isNotEmpty() ? $link->category->ancestors->first() : $link->category;

            if ($root->id == 3) {
                $fields = array_merge($fields, [
                    Text::make('Автор', 'extras->author'),

                    BooleanGroup::make('Форматы', 'extras->formats')->options(function () {
                        return [
                            'print' => 'Печатная',
                            'digital' => 'Электронная',
                            'audio' => 'Аудио'
                        ];
                    }),
                ]);
            }
        }

        return $fields;
    }

    /**
     * Get the cards available for the request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function cards(Request $request)
    {
        return [];
    }

    /**
     * Get the filters available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function filters(Request $request)
    {
        return [
            new Filters\LinkStatus,
            new Filters\LinkCategory
        ];
    }

    /**
     * Get the lenses available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function lenses(Request $request)
    {
        return [];
    }

    /**
     * Get the actions available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function actions(Request $request)
    {
        return [
            new Actions\AssignLinkToDigest,
            new Actions\SendToTelegram,
            new Actions\GenerateLinksImageGallery,
            new Actions\AttachLinkToCategory
        ];
    }
}
