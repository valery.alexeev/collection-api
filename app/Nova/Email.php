<?php

namespace App\Nova;

use Laravel\Nova\Fields\ID;
use Illuminate\Http\Request;
use Laravel\Nova\Fields\Text;
use Laravel\Nova\Fields\Select;
use Laravel\Nova\Fields\Boolean;
use Laravel\Nova\Fields\Markdown;
use Laravel\Nova\Fields\Textarea;
use App\Nova\Actions\DuplicateEmail;
use Jackabox\DuplicateField\DuplicateField;
use Laravel\Nova\Http\Requests\NovaRequest;
use Whitecube\NovaFlexibleContent\Flexible;
use Ctessier\NovaAdvancedImageField\AdvancedImage;

class Email extends Resource
{
    /**
     * The model the resource corresponds to.
     *
     * @var string
     */
    public static $model = \App\Models\Email::class;

    /**
     * The single value that should be used to represent the resource when being displayed.
     *
     * @var string
     */
    public static $title = 'title';

    /**
     * The columns that should be searched.
     *
     * @var array
     */
    public static $search = [
        'id', 'title'
    ];

    /**
     * Get the fields displayed by the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function fields(Request $request)
    {
        $color_scheme = Select::make('Цвет секции', 'color')->options([
            '#ffffff' => 'Белый',
            '#F9FAF7' => 'Светлый'
        ])->default('#ffffff');

        $section_heading = [
            Text::make('Заголовок секции', 'title'),
            Text::make('Описание секции', 'description'),
        ];


        return [
            ID::make(__('ID'), 'id')->sortable(),

            Text::make('Title'),
            Flexible::make('Блоки', 'content->blocks')
                ->addLayout('Интро', 'intro', [
                    Text::make('Emoji', 'emoji')->default('🤟🏻'),
                    ...$section_heading,
                    Boolean::make('Выводить автора', 'show_author'),
                    Textarea::make('Текст приветствия', 'intro_text'),
                    $color_scheme
                ])
                ->addLayout('Простой текст', 'text', [
                    ...$section_heading,
                    $color_scheme,
                    Markdown::make('Text')->alwaysShow(),
                ])
                ->addLayout('Список ссылок', 'links', [
                    
                    ...$section_heading,
                    $color_scheme,
                    
                    Select::make('Лейаут', 'layout')->options([
                        'news' => 'Компактно (список)',
                        'emoji' => 'Подробно (с emoji и картинкой)',
                        'video' => 'Список видео',
                        'checklist' => 'Чеклист',
                        'book' => 'Книги'
                    ]),

                    Flexible::make('Список ссылок', 'links')->addLayout('Список ссылок', 'links', [
                        Text::make('Заголовок', 'title')->required(),
                        Text::make('Ссылка', 'url'),
                        Textarea::make('Описание', 'description'),
                        Text::make('Emoji', 'emoji'),
                        AdvancedImage::make('Картинка', 'image')->croppable()->resize(660)
                    ])->fullWidth()
                ])->fullWidth()
        ];
    }

    /**
     * Get the cards available for the request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function cards(Request $request)
    {
        return [];
    }

    /**
     * Get the filters available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function filters(Request $request)
    {
        return [];
    }

    /**
     * Get the lenses available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function lenses(Request $request)
    {
        return [];
    }

    /**
     * Get the actions available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function actions(Request $request)
    {
        return [
            (new DuplicateEmail())->showOnTableRow()
        ];
    }
}
