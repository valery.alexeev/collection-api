<?php

namespace App\Nova\Actions;

use App\Models\Link;
use Illuminate\Bus\Queueable;
use Laravel\Nova\Fields\Select;
use Laravel\Nova\Actions\Action;
use Illuminate\Support\Collection;
use Laravel\Nova\Fields\ActionFields;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class AttachLinkToCategory extends Action
{
    use InteractsWithQueue, Queueable;

    /**
     * Perform the action on the given models.
     *
     * @param  \Laravel\Nova\Fields\ActionFields  $fields
     * @param  \Illuminate\Support\Collection  $models
     * @return mixed
     */
    public function handle(ActionFields $fields, Collection $models)
    {
        Link::whereIn('id', $models->pluck('id'))->update(['category_id' => $fields->category_id]);
    }

    /**
     * Get the fields available on the action.
     *
     * @return array
     */
    public function fields()
    {
        return [
            Select::make('Категория', 'category_id')
                ->options(function () {
                    return \App\Models\Category
                        ::withDepth()
                        ->defaultOrder()
                        ->get()
                        ->reduce(function ($options, $model) {
                            $options[$model['id']] = $model->nested_title;
                            return $options;
                        }, []);
                }),
        ];
    }
}
