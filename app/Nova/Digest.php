<?php

namespace App\Nova;

use App\Models\Link;
use App\Models\Category;
use Laravel\Nova\Fields\ID;
use Illuminate\Http\Request;
use Laravel\Nova\Fields\Text;
use Laravel\Nova\Fields\Select;
use Laravel\Nova\Fields\HasMany;
use Laravel\Nova\Fields\Textarea;
use App\Nova\Actions\PublishDigest;
use Laravel\Nova\Fields\BooleanGroup;
use App\Nova\Actions\SendDigestToSendpulse;
use Laravel\Nova\Http\Requests\NovaRequest;

class Digest extends Resource
{
    /**
     * The model the resource corresponds to.
     *
     * @var string
     */
    public static $model = \App\Models\Digest::class;

    /**
     * The single value that should be used to represent the resource when being displayed.
     *
     * @var string
     */
    public static $title = 'title';

    /**
     * The columns that should be searched.
     *
     * @var array
     */
    public static $search = [
        'id',
    ];

    /**
     * Get the fields displayed by the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function fields(Request $request)
    {
        return [
            ID::make(__('ID'), 'id')->sortable(),
            Text::make('Title'),
            Textarea::make('Вступление', 'content->intro')->hideFromIndex(),

            HasMany::make('Links', 'links', 'App\Nova\Link'),
            Select::make('Главная книга', 'content->book')->displayUsingLabels()->options(function(){
                $category = Category::where('id', '3')->with('links')->first();
                return $category->links->pluck('title', 'id');
            })->hideFromIndex()
        ];
    }

    /**
     * Get the cards available for the request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function cards(Request $request)
    {
        return [];
    }

    /**
     * Get the filters available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function filters(Request $request)
    {
        return [];
    }

    /**
     * Get the lenses available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function lenses(Request $request)
    {
        return [];
    }

    /**
     * Get the actions available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function actions(Request $request)
    {
        return [ 
            (new SendDigestToSendpulse())->showOnTableRow(),
            (new PublishDigest())->showOnTableRow()
        ];
    }
}
